# Supra (Frontend Docs) (Created with React.Js)

## Steps to install

- Copy and rename the `.env.example` file to `.env`

- Setup the `.env` with the suitable values.

- In the root folder, run

```
yarn install && yarn build
```

- set the root folder in your web server to `/build`
