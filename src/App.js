import "antd/dist/antd.css";
import "./App.scss";
import Routes from "./app/routes";

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
