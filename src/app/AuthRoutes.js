import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";

function AuthRoutes({ component: Component, ...rest }) {
  const token = useSelector((state) => state.auth.token);

  if (!token) {
    return <Redirect to="/login" />;
  }
  return <Route {...rest} component={Component} />;
}

export default AuthRoutes;
