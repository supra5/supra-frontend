import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";

function GuestRoutes({ component: Component, ...rest }) {
  const auth = useSelector((state) => state.auth);

  if (auth.token) {
    if (auth.user.role.name === "admin") {
      return <Redirect to="/admin/dashboard" />;
    } else if (auth.user.role === "merchant") {
      return <Redirect to="/merchant/dashboard" />;
    } else {
      return <Redirect to="/dashboard" />;
    }
  }

  return <Route {...rest} component={Component} />;
}

export default GuestRoutes;
