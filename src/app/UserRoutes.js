import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";

function UserRoutes({ component: Component, ...rest }) {
  const token = useSelector((state) => state.auth.token);
  const user = useSelector((state) => state.auth.user);

  if (!token) {
    return <Redirect to="/login" />;
  }
  if (user.role.name === "merchant") {
    return <Redirect to="/merchant/dashboard" />;
  }

  if (user.role.name === "admin") {
    return <Redirect to="/admin/dashboard" />;
  }

  return <Route {...rest} component={Component} />;
}

export default UserRoutes;
