import React, { lazy, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import PageLoading from "../components/PageLoading";

import GuestRoutes from "./GuestRoutes";
import UserRoutes from "./UserRoutes";
import AdminRoutes from "./AdminRoutes";
import AuthRoutes from "./AuthRoutes";
import MerchantRoutes from "./MerchantRoutes";

const Home = lazy(() => import("../pages/frontend/Home"));
const Login = lazy(() => import("../pages/frontend/Login"));
const SignUp = lazy(() => import("../pages/frontend/SignUp"));
const Airtime = lazy(() => import("../pages/frontend/AirtimeData"));
const Dashboard = lazy(() => import("../pages/clients/Dashboard"));
const BuyAirtime = lazy(() => import("../pages/clients/BuyAirtime"));
const MyPurchases = lazy(() => import("../pages/clients/MyPurchases"));
const BuyData = lazy(() => import("../pages/clients/BuyData"));
const BuyCable = lazy(() => import("../pages/clients/BuyCable"));
const BuyPower = lazy(() => import("../pages/clients/BuyPower"));
const AdminDashboard = lazy(() => import("../pages/admin/Dashboard"));
const PageNotFound = lazy(() => import("../pages/PageNotFound"));
const ForgotPassword = lazy(() => import("../pages/frontend/ForgotPassword"));
const ResetPassword = lazy(() => import("../pages/frontend/ResetPassword"));
const Upgrade = lazy(() => import("../pages/clients/Upgrade"));
const ManageAdmins = lazy(() => import("../pages/admin/ManageAdmins"));
const ManageMerchants = lazy(() => import("../pages/admin/ManageMerchants"));
const ManageClients = lazy(() => import("../pages/admin/ManageClients"));
const Transactions = lazy(() => import("../pages/admin/Transactions"));
const UpdateProfile = lazy(() => import("../pages/auth/UpdateProfile"));
const MerchantDashboard = lazy(() => import("../pages/merchant/Dashboard"));
const MyLink = lazy(() => import("../pages/merchant/MyLink"));
const MerchantSettings = lazy(() => import("../pages/admin/MerchantSettings"));
const MyEarnings = lazy(() => import("../pages/merchant/MyEarnings"));
const BankDetails = lazy(() => import("../pages/merchant/BankDetails"));
const ScratchCard = lazy(() => import("../pages/clients/BuyScratchCard"));

const Routes = () => {
  return (
    <>
      <Suspense fallback={<PageLoading />}>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/airtime-data">
            <Airtime />
          </Route>
          <GuestRoutes exact path="/forgot-password">
            <ForgotPassword />
          </GuestRoutes>
          <GuestRoutes exact path="/reset-password/:token">
            <ResetPassword />
          </GuestRoutes>
          <GuestRoutes exact path="/register">
            <SignUp />
          </GuestRoutes>
          <GuestRoutes exact path="/login">
            <Login />
          </GuestRoutes>
          <UserRoutes exact path="/dashboard">
            <Dashboard />
          </UserRoutes>
          <UserRoutes exact path="/buy-airtime">
            <BuyAirtime />
          </UserRoutes>
          <AuthRoutes exact path="/my-purchases">
            <MyPurchases />
          </AuthRoutes>

          <UserRoutes exact path="/buy-data">
            <BuyData />
          </UserRoutes>
          <UserRoutes exact path="/buy-cable">
            <BuyCable />
          </UserRoutes>
          <UserRoutes exact path="/buy-power">
            <BuyPower />
          </UserRoutes>
          <UserRoutes exact path="/upgrade">
            <Upgrade />
          </UserRoutes>
          <UserRoutes exact path="/buy-waec-scratchcard">
            <ScratchCard />
          </UserRoutes>
          {/* Admin Routes */}
          <AdminRoutes exact path="/admin/dashboard">
            <AdminDashboard />
          </AdminRoutes>
          <AdminRoutes exact path="/admin/manage-admins">
            <ManageAdmins />
          </AdminRoutes>
          <AdminRoutes exact path="/admin/manage-merchants">
            <ManageMerchants />
          </AdminRoutes>
          <AdminRoutes exact path="/admin/manage-clients">
            <ManageClients />
          </AdminRoutes>
          <AdminRoutes exact path="/admin/merchant-settings">
            <MerchantSettings />
          </AdminRoutes>
          <AdminRoutes exact path="/admin/transactions">
            <Transactions />
          </AdminRoutes>
          <MerchantRoutes exact path="/merchant/dashboard">
            <MerchantDashboard />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/link">
            <MyLink />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/transactions">
            <Transactions />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/buy-airtime">
            <BuyAirtime />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/buy-waec-scratchcard">
            <ScratchCard />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/buy-data">
            <BuyData />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/buy-cable">
            <BuyCable />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/buy-power">
            <BuyPower />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/my-earnings">
            <MyEarnings />
          </MerchantRoutes>
          <MerchantRoutes exact path="/merchant/update-bank-details">
            <BankDetails />
          </MerchantRoutes>
          <AuthRoutes exact path="/update-profile">
            <UpdateProfile />
          </AuthRoutes>

          <Route exact path="*">
            <PageNotFound />
          </Route>
        </Switch>
      </Suspense>
    </>
  );
};

export default Routes;
