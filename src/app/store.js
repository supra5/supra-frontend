import { configureStore, combineReducers } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import styleReducer from "../features/style/styleSlice";
import transactionReducer from "../features/transactions/transactionSlice";
import usersReducer from "../features/users/usersSlice";
import marketingReducer from "../features/marketing/marketingSlice";
import settingsReducer from "../features/settings/settingsSlice";

import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth"],
};

const reducers = combineReducers({
  auth: authReducer,
  style: styleReducer,
  transaction: transactionReducer,
  users: usersReducer,
  marketing: marketingReducer,
  settings: settingsReducer,
});

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export default store;
