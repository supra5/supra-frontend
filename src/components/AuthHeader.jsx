import React from "react";
import { Layout, Dropdown, Menu, Button } from "antd";
import { Link } from "react-router-dom";
import { v4 as uuid } from "uuid";
import { DownOutlined } from "@ant-design/icons";

const { Header } = Layout;
const AuthHeader = ({ user, onLogout, collapsed }) => {
  return (
    <Header
      style={{
        backgroundColor: "white",
        padding: 3,
        position: "fixed",
        zIndex: 1,
        width: "100%",
      }}
    >
      <div
        style={{
          height: "100%",
          marginLeft: collapsed
            ? "calc(100% - 252px)"
            : "calc(100% - (200px*2))",
        }}
      >
        <Dropdown
          overlay={() => (
            <>
              <Menu>
                {user.role.name === "client" && (
                  <Menu.Item key={uuid()}>
                    <Link to="/upgrade">Upgrade</Link>
                  </Menu.Item>
                )}
                <Menu.Item key={uuid()}>
                  <Link to="/update-profile">Update Profile</Link>
                </Menu.Item>

                {user.role.name === "merchant" && (
                  <Menu.Item key={uuid()}>
                    <Link to="/merchant/update-bank-details">
                      Update Bank Details
                    </Link>
                  </Menu.Item>
                )}
                <Menu.Item onClick={onLogout} key={uuid()}>
                  Logout
                </Menu.Item>
              </Menu>
            </>
          )}
        >
          <Button type="text">
            Hello ({user.name}) <DownOutlined />
          </Button>
        </Dropdown>
      </div>
    </Header>
  );
};

export default AuthHeader;
