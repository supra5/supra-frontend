import React from "react";
import { Layout, Typography, Menu } from "antd";
import { Link } from "react-router-dom";
import { orange } from "@ant-design/colors";
import { useSelector } from "react-redux";

const { Sider } = Layout;
const { Title } = Typography;

function AuthSidebar({
  location,
  menuItems,
  styles,
  logo,
  halfLogo,
  collapsed,
  toggleCollapse,
}) {
  const user = useSelector((state) => state.auth.user);

  return (
    <>
      <Sider
        breakpoint="lg"
        onCollapse={toggleCollapse}
        collapsible
        width={220}
        style={{
          backgroundColor: "#b1145e",
          overflow: "auto",
          position: "fixed",
          left: 0,
          height: "100vh",
        }}
      >
        <div
          style={{
            height: "40px",
            margin: "16px",
            display: "flex",
            justifyContent: "center",
            borderRadius: "10px",
            background: "rgba(255, 255, 255, 0.3)",
          }}
        >
          {!collapsed ? (
            <img src={logo} style={{ height: "100%" }} alt="Logo" />
          ) : (
            <img src={halfLogo} alt="half logo" />
          )}
        </div>
        <Menu
          style={{
            backgroundColor: "transparent",
            color: "honeydew",
            fontWeight: "bold",
          }}
        >
          {menuItems.map((item) => (
            <Menu.Item
              className={
                item.link === location.pathname
                  ? styles["ant-menu-item-active"]
                  : null
              }
              key={item.key}
              icon={item.icon}
            >
              <Link
                style={{
                  color: item.link === location.pathname ? "purple" : "white",
                }}
                to={item.link}
              >
                {item.name}
              </Link>
            </Menu.Item>
          ))}
        </Menu>
        <br />
        <br />
        {user.role.name === "merchant" && !collapsed && (
          <Title
            level={5}
            style={{
              display: "flex",
              justifyContent: "center",
              color: orange[4],
            }}
          >
            You are a merchant
          </Title>
        )}
      </Sider>
    </>
  );
}

export default AuthSidebar;
