import React from "react";
import { v4 as uuid } from "uuid";
import { Layout, Menu, Button, Grid } from "antd";
import { BarsOutlined } from "@ant-design/icons";
import { yellow } from "@ant-design/colors";
import { Link } from "react-router-dom";

import logo from "../images/logo.png";

const { Header } = Layout;
const { useBreakpoint } = Grid;

const menuItems = [
  {
    title: "Home",
    url: "/",
    key: uuid(),
  },
  {
    title: "Login",
    url: "/login",
    key: uuid(),
  },
  {
    title: "Sign Up",
    url: "/register",
    key: uuid(),
  },
];

export default function FrontendHeader() {
  const screen = useBreakpoint();
  return (
    <>
      <Header
        style={{
          backgroundColor: yellow[5],
          paddingLeft: "0.6em",
          paddingRight: "0.25em",
        }}
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Link to="/">
            <img
              style={{ objectFit: "contain" }}
              alt="logo"
              className="logo"
              src={logo}
            />
          </Link>

          <Menu
            disabledOverflow={true}
            mode="horizontal"
            style={{
              backgroundColor: "transparent",
              borderBottomStyle: "none",
            }}
          >
            {menuItems.map((item) => (
              <React.Fragment key={item.key}>
                {!screen.xs && (
                  <Menu.Item
                    style={{ borderBottomStyle: "none" }}
                    key={item.key}
                  >
                    <Link className="header-menu-link" to={item.url}>
                      {item.title}
                    </Link>
                  </Menu.Item>
                )}
              </React.Fragment>
            ))}
            {screen.xs && (
              <Menu.Item style={{ borderBottomStyle: "none" }} key={uuid()}>
                <Button type="text" icon={<BarsOutlined />} />
              </Menu.Item>
            )}
          </Menu>
        </div>
      </Header>
    </>
  );
}
