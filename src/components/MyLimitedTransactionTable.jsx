import { Typography, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { grey } from "@ant-design/colors";
import { getMyLimitedTransactions } from "../features/transactions/transactionSlice";

const { Title, Text } = Typography;
const columns = [
  {
    title: <Text style={{ fontWeight: "bold" }}>Name</Text>,
    dataIndex: "biller_name",
    align: "center",
  },
  {
    title: <Text style={{ fontWeight: "bold" }}>Type</Text>,
    dataIndex: "biller_type",
    align: "center",
  },
  {
    title: <Text style={{ fontWeight: "bold" }}>Customer</Text>,
    dataIndex: "customer",
    align: "center",
  },
  {
    title: <Text style={{ fontWeight: "bold" }}>Amount</Text>,
    dataIndex: "amount",
    align: "center",
    render: (amount) => {
      return <>&#8358;{amount}</>;
    },
  },
];

const MyLimitedTransactionTable = () => {
  const dispatch = useDispatch();
  const [transactions, setTransactions] = useState([]);
  const loading = useSelector((state) => state.transaction.loading);
  useEffect(() => {
    (() => {
      dispatch(getMyLimitedTransactions())
        .unwrap()
        .then((res) => {
          setTransactions(res);
        });
    })();
  }, [dispatch]);

  const handleTableChange = ({ current, pageSize }) => {
    dispatch(getMyLimitedTransactions())
      .unwrap()
      .then((res) => {
        setTransactions(res);
      });
  };

  return (
    <>
      <Title level={5} style={{ marginTop: "3em", color: grey[4] }}>
        Transaction History
      </Title>
      <Table
        bordered
        onChange={handleTableChange}
        columns={columns}
        dataSource={transactions}
        rowKey={(transaction) => transaction.id}
        loading={loading}
      />
    </>
  );
};

export default MyLimitedTransactionTable;
