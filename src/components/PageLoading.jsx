import React from "react";
import { Row } from "antd";
import { blue } from "@ant-design/colors";
import { LoadingOutlined } from "@ant-design/icons";

export default function PageLoading() {
  return (
    <Row
      style={{ height: "100vh", backgroundColor: "honeydew", color: blue[4] }}
      justify="center"
      align="middle"
    >
      <LoadingOutlined style={{ fontSize: "6em" }} />
    </Row>
  );
}
