import { Card, Typography } from "antd";
import { useSelector } from "react-redux";

const { Title } = Typography;
export const CardWidget = ({ children }) => {
  return (
    <Card
      style={{
        backgroundColor: "white",
        borderRadius: 5,
        maxHeight: 150,
      }}
    >
      {children}
    </Card>
  );
};

export const TitleWidget = ({ children }) => {
  const globalStyle = useSelector((state) => state.style);
  return (
    <Title
      style={{
        color: globalStyle.secondaryColor,
      }}
      level={5}
    >
      {children}
    </Title>
  );
};

export const StatusWidget = ({ icon, title, value }) => (
  <>
    <CardWidget>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <span style={{ marginTop: -15, marginBottom: 5 }}>{icon}</span>
        <TitleWidget> {value}</TitleWidget>
        <span style={{ textAlign: "center" }}>{title}</span>
      </div>
    </CardWidget>
  </>
);
