import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import api from "../../services/api";
import $api from "../../services/api";

export const login = createAsyncThunk(
    "auth/doLogin",
    async (credentials, { rejectWithValue }) => {
        try {
            const response = await $api.post("/login", {
                email: credentials.email,
                password: credentials.password,
            });
            return response.data;
        } catch (error) {
            if (error.response) {
                return rejectWithValue(error.response.data);
            } else {
                return rejectWithValue({ message: "Server error" });
            }
        }
    }
);

export const logout = createAsyncThunk(
    "auth/doLogout",
    async (_, { dispatch, rejectWithValue }) => {
        try {
            const response = await $api.post("/logout");
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export const getAuthUser = createAsyncThunk(
    "auth/getUser",
    async (_, { rejectWithValue }) => {
        try {
            const response = await api.get("/user");
            return response.data;
        } catch (error) {
            return rejectWithValue(error.response.data);
        }
    }
);

export const requestPasswordReset = createAsyncThunk(
    "auth/requestPasswordReset",
    async ({ email }, { rejectWithValue }) => {
        try {
            const response = await api.post("/forgot-password", { email });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const resetPassword = createAsyncThunk(
    "auth/resetPassword",
    async (
        { email, token, password, password_confirmation },
        { rejectWithValue }
    ) => {
        try {
            const response = await api.post("/reset-password", {
                email,
                token,
                password,
                password_confirmation,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const signUpUser = createAsyncThunk(
    "auth/signUp",
    async (
        { email, password, password_confirmation, phone, name },
        { rejectWithValue }
    ) => {
        try {
            const response = await $api.post("/register", {
                email,
                password,
                password_confirmation,
                phone,
                name,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const updateProfile = createAsyncThunk(
    "auth/updateProfile",
    async ({ email, phone, name }, { rejectWithValue }) => {
        try {
            const response = await $api.post("/update-profile", {
                email,
                phone,
                name,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const changePassword = createAsyncThunk(
    "auth/changePassword",
    async (
        { oldPassword, newPassword, newPassword_confirmation },
        { rejectWithValue }
    ) => {
        try {
            const response = await $api.post("/change-password", {
                oldPassword,
                newPassword,
                newPassword_confirmation,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const generateLink = createAsyncThunk(
    "auth/generateLink",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.post("/generate-link");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const updateBankSettings = createAsyncThunk(
    "auth/updateUpdateBankSettings",
    async ({ bank, account_number }, { rejectWithValue }) => {
        try {
            const response = await $api.post("/update-bank-settings", {
                bank,
                account_number,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const withdrawEarning = createAsyncThunk(
    "auth/withdrawEarning",
    async ({ amount }, { rejectWithValue }) => {
        try {
            const response = await $api.post("/withdraw-earning", {
                amount,
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const deleteSavedCard = createAsyncThunk(
    "auth/deleteSavedCard",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.post("/delete-saved-card");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

const authSlice = createSlice({
    name: "auth",
    initialState: {
        token: null,
        user: null,
        loading: false,
    },
    reducers: {},
    extraReducers: (builder) => {
        //login user
        builder.addCase(login.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(login.fulfilled, (state, action) => {
            state.token = action.payload.token;
            state.user = action.payload.user;
            state.loading = false;
        });
        builder.addCase(login.rejected, (state) => {
            state.loading = false;
            state.token = null;
        });

        //logout user
        builder.addCase(logout.fulfilled, (state) => {
            state.user = null;
            state.loading = false;
            state.token = null;
        });
        builder.addCase(logout.pending, (state) => {
            state.loading = true;
        });

        builder.addCase(logout.rejected, (state) => {
            state.loading = false;
        });

        //Get authenticated user
        builder.addCase(getAuthUser.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getAuthUser.fulfilled, (state, action) => {
            state.user = action.payload.user;
            state.loading = false;
        });
        builder.addCase(getAuthUser.rejected, (state) => {
            state.loading = false;
            state.token = null;
            state.user = null;
        });

        // Request reset password token
        builder.addCase(requestPasswordReset.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(requestPasswordReset.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(requestPasswordReset.rejected, (state) => {
            state.loading = false;
        });

        //Reset Password
        builder.addCase(resetPassword.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(resetPassword.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(resetPassword.pending, (state) => {
            state.loading = true;
        });

        //Reset Password
        builder.addCase(signUpUser.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(signUpUser.fulfilled, (state, action) => {
            state.loading = false;
            state.token = action.payload.token;
            state.user = action.payload.user;
        });
        builder.addCase(signUpUser.pending, (state) => {
            state.loading = true;
        });

        //Update Profile
        builder.addCase(updateProfile.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(updateProfile.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
        });
        builder.addCase(updateProfile.pending, (state) => {
            state.loading = true;
        });

        //Change Password
        builder.addCase(changePassword.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(changePassword.fulfilled, (state, action) => {
            state.loading = false;
            state.token = action.payload.token;
        });
        builder.addCase(changePassword.pending, (state) => {
            state.loading = true;
        });

        //Generate Link
        builder.addCase(generateLink.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(generateLink.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
        });
        builder.addCase(generateLink.pending, (state) => {
            state.loading = true;
        });

        //Update bank settings
        builder.addCase(updateBankSettings.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(updateBankSettings.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
        });
        builder.addCase(updateBankSettings.pending, (state) => {
            state.loading = true;
        });

        //Withdraw earning
        builder.addCase(withdrawEarning.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(withdrawEarning.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
        });
        builder.addCase(withdrawEarning.pending, (state) => {
            state.loading = true;
        });

        //Delete Saved Card
        builder.addCase(deleteSavedCard.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteSavedCard.fulfilled, (state, action) => {
            state.loading = false;
            state.user = action.payload.user;
        });
        builder.addCase(deleteSavedCard.pending, (state) => {
            state.loading = true;
        });
    },
});

export default authSlice.reducer;
