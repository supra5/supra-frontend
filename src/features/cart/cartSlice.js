import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
    name: "cart",
    initialState: {
        selectedNetwork: null,
        billerName: null,
        amount: 0,
    },
    reducers: {
        selectNetwork: (state, action) => {
            state.selectedNetwork = action.payload;
        },
    },
});

export const { selectNetwork } = cartSlice.actions;

export default cartSlice.reducer;
