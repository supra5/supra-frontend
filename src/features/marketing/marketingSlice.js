import { createSlice } from "@reduxjs/toolkit";

const marketingSlice = createSlice({
    name: "marketing",
    initialState: {
        merchantLink: null,
    },
    reducers: {
        setMerchantLink: (state, action) => {
            state.merchantLink = action.payload;
        },
    },
    extraReducers: (builder) => {},
});

export const { setMerchantLink } = marketingSlice.actions;
export default marketingSlice.reducer;
