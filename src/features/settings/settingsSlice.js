import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import $api from "../../services/api";

export const getMerchantSettings = createAsyncThunk(
    "settings/getMerchantSettings",
    async (_, { rejectWithValue }) => {
        try {
            const res = await $api.get("/get-merchant-settings");
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const updateMerchantSettings = createAsyncThunk(
    "settings/updateMerchantSettings",
    async ({ commissionPercentage, upgradeSum }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/update-merchant-settings", {
                commissionPercentage,
                upgradeSum,
            });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

const settingsSlice = createSlice({
    name: "settings",
    initialState: {
        merchantSetting: {
            loading: false,
            commissionPercentage: 0,
            upgradeSum: 0,
        },
    },
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getMerchantSettings.fulfilled, (state, action) => {
            state.merchantSetting.commissionPercentage =
                action.payload.commissionPercentage;
            state.merchantSetting.upgradeSum = action.payload.upgradeSum;
            state.merchantSetting.loading = false;
        });
        builder.addCase(getMerchantSettings.pending, (state) => {
            state.merchantSetting.loading = true;
        });
        builder.addCase(getMerchantSettings.rejected, (state) => {
            state.merchantSetting.loading = false;
        });
        builder.addCase(updateMerchantSettings.fulfilled, (state, action) => {
            state.merchantSetting.loading = false;
            state.merchantSetting.commissionPercentage =
                action.payload.commissionPercentage;
            state.merchantSetting.upgradeSum = action.payload.upgradeSum;
        });
        builder.addCase(updateMerchantSettings.pending, (state) => {
            state.merchantSetting.loading = true;
        });
        builder.addCase(updateMerchantSettings.rejected, (state) => {
            state.merchantSetting.loading = false;
        });
    },
});

export default settingsSlice.reducer;
