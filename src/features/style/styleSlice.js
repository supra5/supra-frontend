import { createSlice } from "@reduxjs/toolkit";
import { grey } from "@ant-design/colors";

const styleSlice = createSlice({
    name: "style",
    initialState: {
        primaryColor: "#9f1254",
        secondaryColor: grey[4],
    },
});

// export const {} = styleSlice.actions;

export default styleSlice.reducer;
