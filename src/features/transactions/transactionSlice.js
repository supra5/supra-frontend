import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import $api from "../../services/api";

export const getMyTransactions = createAsyncThunk(
    "transactions/getMyTransaction",
    async ({ page, perPage }, { rejectWithValue }) => {
        try {
            const response = await $api.get("/my-transactions", {
                params: { page, perPage },
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const getMyLimitedTransactions = createAsyncThunk(
    "transactions/getMyLimitedTransaction",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.get("/my-limited-transactions");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const getLimitedTransactions = createAsyncThunk(
    "transactions/getLimitedTransaction",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.get("/limited-transactions");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const getAllTransactions = createAsyncThunk(
    "transactions/getAllTransactions",
    async ({ page, perPage }, { rejectWithValue }) => {
        try {
            const response = await $api.get("/all-transactions", {
                params: { page, perPage },
            });
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const getTotalTransactions = createAsyncThunk(
    "transactions/getTotalTransactions",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.get("/total-transactions");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

export const getAllBanks = createAsyncThunk(
    "transactions/getAllBanks",
    async (_, { rejectWithValue }) => {
        try {
            const response = await $api.get("/get-all-banks");
            return response.data;
        } catch (error) {
            return rejectWithValue(JSON.stringify(error.response));
        }
    }
);

const transactionSlice = createSlice({
    name: "transaction",
    initialState: {
        loading: false,
        totalTransactions: 0.0,
        limitedTransactions: [],
        banks: [],
    },
    extraReducers: (builder) => {
        builder.addCase(getMyTransactions.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(getMyTransactions.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getMyTransactions.rejected, (state) => {
            state.loading = false;
        });

        builder.addCase(getMyLimitedTransactions.fulfilled, (state, action) => {
            state.loading = false;
            state.limitedTransactions = action.payload.transactions;
        });
        builder.addCase(getMyLimitedTransactions.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getMyLimitedTransactions.rejected, (state) => {
            state.loading = false;
        });

        builder.addCase(getAllTransactions.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllTransactions.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllTransactions.pending, (state) => {
            state.loading = true;
        });
        //limited transactions
        builder.addCase(getLimitedTransactions.fulfilled, (state, action) => {
            state.limitedTransactions = action.payload.transactions;
            state.loading = false;
        });
        builder.addCase(getLimitedTransactions.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(getLimitedTransactions.pending, (state) => {
            state.loading = true;
        });

        //total transactions
        builder.addCase(getTotalTransactions.fulfilled, (state, action) => {
            state.loading = false;
            state.totalTransactions = action.payload.amount;
        });
        builder.addCase(getTotalTransactions.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(getTotalTransactions.pending, (state) => {
            state.loading = true;
        });

        //get all banks
        //total transactions
        builder.addCase(getAllBanks.fulfilled, (state, action) => {
            state.loading = false;
            state.banks = action.payload.banks;
        });
        builder.addCase(getAllBanks.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllBanks.pending, (state) => {
            state.loading = true;
        });
    },
});

export default transactionSlice.reducer;
