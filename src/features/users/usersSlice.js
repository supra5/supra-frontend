import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import $api from "../../services/api";

export const getAllClients = createAsyncThunk(
    "users/getAllClients",
    async ({ page, perPage }, { rejectWithValue }) => {
        try {
            const res = await $api.get("/users/get-clients", {
                params: { page, perPage },
            });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const addClient = createAsyncThunk(
    "users/addClient",
    async ({ name, email, phone, password }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/add-client", {
                name,
                email,
                phone,
                password,
            });

            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const deleteClient = createAsyncThunk(
    "users/deleteClient",
    async ({ id }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/delete-client", { id });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const getAllMerchants = createAsyncThunk(
    "users/getAllMerchants",
    async ({ page, perPage }, { rejectWithValue }) => {
        try {
            const res = await $api.get("/users/get-merchants", {
                params: { page, perPage },
            });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const addNewMerchant = createAsyncThunk(
    "users/addNewMerchant",
    async ({ name, email, phone, password }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/add-merchant", {
                name,
                email,
                phone,
                password,
            });

            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const deleteMerchant = createAsyncThunk(
    "users/deleteMerchant",
    async ({ id }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/delete-merchant", { id });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const getAllAdmins = createAsyncThunk(
    "users/getAllAdmins",
    async ({ page, perPage }, { rejectWithValue }) => {
        try {
            const res = await $api.get("/users/get-admins", {
                params: { page, perPage },
            });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const addNewAdmin = createAsyncThunk(
    "users/addNewAdmin",
    async ({ name, email, phone, password }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/add-admin", {
                name,
                email,
                phone,
                password,
            });

            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const deleteAdmin = createAsyncThunk(
    "users/deleteAdmin",
    async ({ id }, { rejectWithValue }) => {
        try {
            const res = await $api.post("/users/delete-admin", { id });
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const countMerchants = createAsyncThunk(
    "users/countMerchants",
    async (_, { rejectWithValue }) => {
        try {
            const res = await $api.get("/users/count-merchants");
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

export const countClients = createAsyncThunk(
    "users/countUsers",
    async (_, { rejectWithValue }) => {
        try {
            const res = await $api.get("/users/count-clients");
            return res.data;
        } catch (err) {
            return rejectWithValue(JSON.stringify(err.response));
        }
    }
);

const userSlice = createSlice({
    name: "users",
    initialState: {
        loading: false,
        merchantsCount: 0,
        clientsCount: 0,
    },
    extraReducers: (builder) => {
        builder.addCase(getAllClients.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllClients.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getAllClients.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(addClient.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(addClient.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(addClient.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteClient.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteClient.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(deleteClient.rejected, (state) => {
            state.loading = false;
        });

        //delete admin
        builder.addCase(deleteAdmin.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteAdmin.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(deleteAdmin.rejected, (state) => {
            state.loading = false;
        });
        //create admin
        builder.addCase(addNewAdmin.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(addNewAdmin.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(addNewAdmin.rejected, (state) => {
            state.loading = false;
        });
        //list admin
        builder.addCase(getAllAdmins.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllAdmins.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getAllAdmins.rejected, (state) => {
            state.loading = false;
        });

        //getMerchant
        builder.addCase(getAllMerchants.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(getAllMerchants.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getAllMerchants.rejected, (state) => {
            state.loading = false;
        });
        //createMerchant
        builder.addCase(addNewMerchant.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(addNewMerchant.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(addNewMerchant.rejected, (state) => {
            state.loading = false;
        });
        //deleteMerchant
        builder.addCase(deleteMerchant.fulfilled, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteMerchant.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(deleteMerchant.rejected, (state) => {
            state.loading = false;
        });

        //count users
        builder.addCase(countClients.fulfilled, (state, action) => {
            state.clientsCount = action.payload.count;
            state.loading = false;
        });
        builder.addCase(countClients.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(countClients.rejected, (state) => {
            state.loading = false;
        });
        //count merhcants
        builder.addCase(countMerchants.fulfilled, (state, action) => {
            state.merchantsCount = action.payload.count;
            state.loading = false;
        });
        builder.addCase(countMerchants.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(countMerchants.rejected, (state) => {
            state.loading = false;
        });
    },
});

export default userSlice.reducer;
