import { useEffect, useState } from "react";
const usePageTitle = (title = "Welcome") => {
  const [docTitle, setDocTitle] = useState(title);
  useEffect(() => {
    document.title = docTitle + " | Supra";
  }, [docTitle]);

  return [docTitle, setDocTitle];
};

export { usePageTitle };
