import { Layout, notification } from "antd";
import {
  DashboardOutlined,
  DollarCircleOutlined,
  PhoneOutlined,
  PlayCircleFilled,
  WifiOutlined,
  BulbOutlined,
  UserOutlined,
  MoneyCollectOutlined,
  LinkOutlined,
  SettingOutlined,
  KeyOutlined,
} from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import { v4 as uuid } from "uuid";
import logo from "../images/logo.png";
import halfLogo from "../images/half_logo.png";
import styles from "../styles/authLayout.module.css";
import AuthSidebar from "../components/AuthSidebar";
import { useSelector, useDispatch } from "react-redux";

import { getAuthUser, logout } from "../features/auth/authSlice";
import AuthHeader from "../components/AuthHeader";

const { Content } = Layout;

const userMenuItems = [
  {
    name: "Dashboard",
    link: "/dashboard",
    icon: <DashboardOutlined />,
    key: uuid(),
  },
  {
    name: "My Purchases",
    link: "/my-purchases",
    icon: <DollarCircleOutlined />,
    key: uuid(),
  },
  {
    name: "Buy Airtime",
    link: "/buy-airtime",
    icon: <PhoneOutlined />,
    key: uuid(),
  },

  {
    name: "Buy Data Plan",
    link: "/buy-data",
    icon: <WifiOutlined />,
    key: uuid(),
  },
  {
    name: "Buy Cable Plan",
    link: "/buy-cable",
    icon: <PlayCircleFilled />,
    key: uuid(),
  },
  {
    name: "Recharge Power",
    link: "/buy-power",
    key: uuid(),
    icon: <BulbOutlined />,
  },
  {
    name: "Buy Waec Scratchcard",
    link: "/buy-waec-scratchcard",
    key: uuid(),
    icon: <KeyOutlined />,
  },
];

const adminMenuItems = [
  {
    name: "Dashboard",
    link: "/admin/dashboard",
    key: uuid(),
    icon: <DashboardOutlined />,
  },
  {
    name: "Manage Merchants",
    link: "/admin/manage-merchants",
    key: uuid(),
    icon: <UserOutlined />,
  },
  {
    name: "Manage Clients",
    link: "/admin/manage-clients",
    key: uuid(),
    icon: <UserOutlined />,
  },
  {
    name: "Merchant Settings",
    link: "/admin/merchant-settings",
    key: uuid(),
    icon: <SettingOutlined />,
  },
  {
    name: "All Transactions",
    link: "/admin/transactions",
    key: uuid(),
    icon: <DollarCircleOutlined />,
  },
  {
    name: "Manage Admins",
    link: "/admin/manage-admins",
    key: uuid(),
    icon: <UserOutlined />,
  },
];

const merchantMenuItems = [
  {
    name: "Dashboard",
    link: "/merchant/dashboard",
    key: uuid(),
    icon: <MoneyCollectOutlined />,
  },
  {
    name: "My Transactions",
    link: "/my-purchases",
    key: uuid(),
    icon: <DollarCircleOutlined />,
  },

  {
    name: "My Earnings",
    link: "/merchant/my-earnings",
    key: uuid(),
    icon: <DollarCircleOutlined />,
  },
  {
    name: "My Link",
    link: "/merchant/link",
    key: uuid(),
    icon: <LinkOutlined />,
  },
];

function AuthLayout({ children }) {
  const dispatch = useDispatch();
  const location = useLocation();
  const user = useSelector((state) => state.auth.user);
  const history = useHistory();

  const [collapsed, setCollapsed] = useState(false);
  const toggleCollapse = () => {
    setCollapsed(!collapsed);
  };
  let menuItems;
  if (user.role.name === "admin") {
    menuItems = adminMenuItems;
  } else if (user.role.name === "merchant") {
    menuItems = merchantMenuItems;
  } else {
    menuItems = userMenuItems;
  }
  const onLogout = async () => {
    if (window.confirm("Are you sure")) {
      await dispatch(logout());
      history.replace("/login");
    }
  };

  useEffect(() => {
    dispatch(getAuthUser())
      .unwrap()
      .catch(() => {
        notification.error({
          message: "Your session has expired.",
          description: "Please login again",
          duration: 1.5,
        });
      });
  }, [dispatch]);

  return (
    <>
      <Layout>
        <AuthSidebar
          location={location}
          styles={styles}
          collapsed={collapsed}
          toggleCollapse={toggleCollapse}
          menuItems={menuItems}
          logo={logo}
          halfLogo={halfLogo}
        />
        <Layout style={{ marginLeft: collapsed ? 81 : 220 }}>
          <AuthHeader collapsed={collapsed} onLogout={onLogout} user={user} />
          <Content style={{ backgroundColor: "#fff", marginTop: 64 }}>
            {children}
          </Content>
          {/* <Footer
            style={{ textAlign: "center", fontWeight: "bold", color: grey[4] }}
          >
            Design By AGZ
          </Footer> */}
        </Layout>
      </Layout>
    </>
  );
}

export default AuthLayout;
