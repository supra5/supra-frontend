import React, { useEffect } from "react";
import { Layout } from "antd";
import { useLocation, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setMerchantLink } from "../features/marketing/marketingSlice";

import FrontendHeader from "../components/FrontendHeader";
const { Header, Content } = Layout;
export default function FrontendLayout({ children }) {
  // const merchantLink = useSelector((state) => state.marketing.merchantCode);
  const location = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    if (searchParams.has("link")) {
      dispatch(
        setMerchantLink(
          window.location.protocol +
            "//" +
            window.location.host +
            location.search
        )
      );
      history.replace("/dashboard");
    }
    return () => {};
  }, [location, dispatch, history]);
  return (
    <>
      <Layout>
        <Layout>
          <FrontendHeader />
        </Layout>
        <Layout>
          <Header style={{ backgroundColor: "black" }}></Header>
        </Layout>
        <Content>{children}</Content>
      </Layout>
    </>
  );
}
