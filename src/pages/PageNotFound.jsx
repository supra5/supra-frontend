import { Typography } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import AuthLayout from "../layouts/AuthLayout";
import FrontendLayout from "../layouts/FrontendLayout";
import { usePageTitle } from "../hooks/usePageTitle";

function PageNotFound() {
    usePageTitle("Page Not Found");
    const token = useSelector((state) => state.auth.token);
    if (!token) {
        return (
            <FrontendLayout>
                <Typography.Title level={3} style={{ textAlign: "center" }}>
                    Page Not Found
                </Typography.Title>
            </FrontendLayout>
        );
    } else {
        return (
            <AuthLayout>
                <Typography.Title level={3} style={{ textAlign: "center" }}>
                    Page Not Found
                </Typography.Title>
            </AuthLayout>
        );
    }
}

export default PageNotFound;
