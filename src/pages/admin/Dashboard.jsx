import React, { useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { Row, Col, Card, Typography, Table } from "antd";
import { useSelector } from "react-redux";
import { formatNumber } from "../../utils/helpers";
import { useDispatch } from "react-redux";
import { countClients, countMerchants } from "../../features/users/usersSlice";
import { blue, green, grey, orange } from "@ant-design/colors";
import {
    getLimitedTransactions,
    getTotalTransactions,
} from "../../features/transactions/transactionSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

const CardWidget = ({ children, bgColor }) => {
    const globalStyle = useSelector((state) => state.style);
    return (
        <Card
            style={{
                backgroundColor: bgColor ? bgColor : globalStyle.primaryColor,
                borderRadius: 10,
                minHeight: "9rem",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: "3em",
            }}
        >
            <Title style={{ color: "white", textAlign: "center" }} level={4}>
                {children}
            </Title>
        </Card>
    );
};

const TitleWidget = ({ children }) => {
    const globalStyle = useSelector((state) => state.style);
    return (
        <Title
            style={{
                textAlign: "center",
                color: globalStyle.secondaryColor,
            }}
            level={5}
        >
            {children}
        </Title>
    );
};

const widgetSpacing = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 4,
};

const columns = [
    {
        title: () => (
            <Title style={{ color: grey[5] }} level={5}>
                Name
            </Title>
        ),
        dataIndex: "name",
        render: (_, transaction) => transaction.user.name,
    },
    {
        title: (
            <Title style={{ color: grey[5] }} level={5}>
                Amount
            </Title>
        ),
        dataIndex: "amount",
    },
    {
        title: (
            <Title style={{ color: grey[5] }} level={5}>
                Biller Name
            </Title>
        ),
        dataIndex: "biller_name",
    },
    {
        title: (
            <Title style={{ color: grey[5] }} level={5}>
                Customer
            </Title>
        ),
        dataIndex: "customer",
    },
    {
        title: (
            <Title style={{ color: grey[5] }} level={5}>
                Time of Transaction
            </Title>
        ),
        dataIndex: "time_of_purchase",
    },
];

function AdminDashoard() {
    usePageTitle("Dashboard");
    const clientsCount = useSelector((state) => state.users.clientsCount);
    const merchantsCount = useSelector((state) => state.users.merchantsCount);
    const totalTransactions = useSelector(
        (state) => state.transaction.totalTransactions
    );
    const transactionsLoading = useSelector(
        (state) => state.transaction.loading
    );
    const limitedTransactions = useSelector(
        (state) => state.transaction.limitedTransactions
    );
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(countMerchants());
        dispatch(countClients());
        dispatch(getTotalTransactions());
        dispatch(getLimitedTransactions());
    }, [dispatch]);
    return (
        <AuthLayout>
            <div style={{ padding: "2em" }}>
                <Row gutter={5}>
                    <Col {...widgetSpacing}>
                        <TitleWidget>Transactions</TitleWidget>
                        <CardWidget bgColor={green[3]}>
                            &#8358;{" "}
                            {totalTransactions > 0
                                ? formatNumber(totalTransactions)
                                : "0"}
                        </CardWidget>
                    </Col>
                    <Col {...widgetSpacing}>
                        <TitleWidget>Clients</TitleWidget>
                        <CardWidget {...widgetSpacing} bgColor={orange[5]}>
                            {clientsCount}
                        </CardWidget>
                    </Col>
                    <Col {...widgetSpacing}>
                        <TitleWidget> Merchants</TitleWidget>
                        <CardWidget bgColor={[blue[4]]}>
                            {merchantsCount}
                        </CardWidget>
                    </Col>
                    <Col xs={24}>
                        <Card>
                            <Title style={{ color: grey[5] }} level={4}>
                                Usage Summary
                            </Title>
                            <Table
                                loading={transactionsLoading}
                                columns={columns}
                                dataSource={limitedTransactions}
                                rowKey={(t) => t.id}
                                pagination={false}
                                bordered
                            />
                        </Card>
                    </Col>
                </Row>
            </div>
        </AuthLayout>
    );
}

export default AdminDashoard;
