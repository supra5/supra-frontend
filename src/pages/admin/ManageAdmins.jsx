import React, { useEffect, useState } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Row,
    Col,
    Button,
    Typography,
    Table,
    Card,
    Form,
    Input,
    notification,
    Modal,
} from "antd";
import { orange, red, green } from "@ant-design/colors";
import { useSelector, useDispatch } from "react-redux";
import {
    deleteAdmin,
    getAllAdmins,
    addNewAdmin,
} from "../../features/users/usersSlice";
import moment from "moment";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

function ManageAdmins() {
    usePageTitle("Manage Admins");
    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            align: "center",
            // responsive: ["xs"],
        },
        {
            title: "E-mail",
            dataIndex: "email",
            align: "center",
        },
        { title: "Phone", dataIndex: "phone", align: "center" },
        {
            title: "Date Created",
            dataIndex: "created_at",
            align: "center",
            render: (text) => {
                return moment(text).format("Y-m-d H:m:s");
            },
        },
        {
            title: "Actions",
            dataIndex: "actions",
            align: "center",
            render: (_, admin) => {
                return (
                    <>
                        <Button
                            size="small"
                            style={{
                                backgroundColor: green[5],
                                border: 0,
                                color: "honeydew",
                                marginBottom: 4,
                            }}
                            onClick={() => handleSelectAdmin(admin)}
                        >
                            View
                        </Button>
                        &nbsp;
                        <Button
                            size="small"
                            style={{
                                backgroundColor: red[5],
                                border: 0,
                                color: "honeydew",
                                marginBottom: 4,
                            }}
                            onClick={() => {
                                if (window.confirm("Are you sure?")) {
                                    dispatch(deleteAdmin({ id: admin.id }))
                                        .unwrap()
                                        .then(() => {
                                            notification.info({
                                                message:
                                                    "Admin deleted successfully",
                                            });
                                            handleTableChange({
                                                current: pagination.current,
                                                pageSize: pagination.pageSize,
                                            });
                                        });
                                }
                            }}
                        >
                            Delete
                        </Button>
                    </>
                );
            },
        },
    ];

    const [addAdminForm] = Form.useForm();
    const { Item } = Form;
    const style = useSelector((state) => state.style);
    const loading = useSelector((state) => state.users.loading);
    const dispatch = useDispatch();
    const [admins, setAdmins] = useState([]);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 1,
        total: 0,
        position: ["bottomRight"],
    });

    const handleTableChange = ({ current, pageSize }) => {
        dispatch(getAllAdmins({ page: current, perPage: pageSize }))
            .unwrap()
            .then((res) => {
                setAdmins(res.data);
                setPagination({
                    ...pagination,
                    current: res.current_page,
                    pageSize: res.per_page,
                    total: res.total,
                });
            });
    };
    useEffect(() => {
        (() => {
            dispatch(getAllAdmins({ page: 1, perPage: 20 }))
                .unwrap()
                .then((res) => {
                    setAdmins(res.data);
                    setPagination({
                        current: res.current_page,
                        pageSize: res.per_page,
                        total: res.total,
                        position: ["bottomRight"],
                    });
                });
        })();
    }, [dispatch]);

    const [selectedAdmin, setSelectedAdmin] = useState(null);
    const [isModalVisble, setIsModalVisible] = useState(false);

    const handleSelectAdmin = (admin) => {
        setSelectedAdmin(admin);
        showModal();
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setSelectedAdmin(null);
        setIsModalVisible(false);
    };

    const onSubmit = ({ name, email, phone, password }) => {
        dispatch(addNewAdmin({ name, email, phone, password }))
            .unwrap()
            .then(() => {
                notification.success({
                    message: "Admin added successfully",
                });
                handleTableChange({
                    current: pagination.current,
                    pageSize: pagination.pageSize,
                });
                addAdminForm.resetFields();
            })
            .catch((err) => {
                const error = JSON.parse(err);
                if (error.status === 422) {
                    let errors = error.data.errors;
                    for (err in errors) {
                        addAdminForm.setFields([
                            { name: err, errors: errors[err] },
                        ]);
                    }
                } else {
                    notification.error({
                        message: "Server error. Please try again later",
                    });
                }
            });
    };
    return (
        <AuthLayout>
            <div style={{ padding: "0 5px" }}>
                <Modal
                    title="Admin Details"
                    visible={isModalVisble}
                    onOk={handleOk}
                    onCancel={handleOk}
                >
                    <Row gutter={8}>
                        <Col xs={24} md={8}>
                            <b>Name</b>: {selectedAdmin?.name}
                        </Col>
                        <Col xs={24} md={8}>
                            <b>Email</b>: {selectedAdmin?.email}
                        </Col>
                        <Col xs={24} md={8}>
                            <b>Phone number</b>: {selectedAdmin?.phone}
                        </Col>
                    </Row>
                </Modal>
                <Row gutter={5}>
                    <Col xs={24} lg={8}>
                        <Card size="small">
                            <Title
                                level={4}
                                style={{
                                    color: style.secondaryColor,
                                    marginLeft: 5,
                                }}
                            >
                                Add an Admin
                            </Title>
                            <Form
                                layout="vertical"
                                form={addAdminForm}
                                onFinish={onSubmit}
                            >
                                <Item
                                    name="name"
                                    label="Name"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Field required",
                                        },
                                    ]}
                                >
                                    <Input />
                                </Item>
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message: "Field required",
                                        },
                                    ]}
                                    name="email"
                                    label="E-mail"
                                >
                                    <Input />
                                </Item>
                                <Item
                                    name="phone"
                                    label="Phone"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Field required",
                                        },
                                    ]}
                                >
                                    <Input />
                                </Item>
                                <Item
                                    name="password"
                                    label="Password"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Field required",
                                        },
                                    ]}
                                >
                                    <Input.Password />
                                </Item>
                                <Item>
                                    <Button
                                        loading={loading}
                                        style={{
                                            border: 0,
                                            backgroundColor: orange[4],
                                            color: "white",
                                        }}
                                        block
                                        htmlType="submit"
                                    >
                                        Add Admin
                                    </Button>
                                </Item>
                            </Form>
                        </Card>
                    </Col>
                    <Col xs={24} lg={16}>
                        <Card size="small">
                            <Title
                                style={{
                                    color: style.secondaryColor,
                                    marginLeft: 5,
                                }}
                                level={4}
                            >
                                Manage Administrators
                            </Title>
                            <div style={{ overflowX: "auto" }}>
                                <Table
                                    size="small"
                                    pagination={pagination}
                                    bordered
                                    columns={columns}
                                    dataSource={admins}
                                    onChange={handleTableChange}
                                    rowKey={(c) => c.id}
                                    loading={loading}
                                />
                            </div>
                        </Card>
                    </Col>
                </Row>
            </div>
        </AuthLayout>
    );
}

export default ManageAdmins;
