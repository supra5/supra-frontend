import React, { useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Row,
    Col,
    Card,
    Typography,
    Form,
    Input,
    Button,
    notification,
} from "antd";
import { grey, orange } from "@ant-design/colors";
import { useDispatch, useSelector } from "react-redux";
import {
    getMerchantSettings,
    updateMerchantSettings,
} from "../../features/settings/settingsSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

const { Item } = Form;
function MerchantSettings() {
    usePageTitle("Merchant Settings");
    const dispatch = useDispatch();

    const merchantSetting = useSelector(
        (state) => state.settings.merchantSetting
    );
    const [merchantSettingsForm] = Form.useForm();
    useEffect(() => {
        dispatch(getMerchantSettings())
            .unwrap()
            .then((res) => {
                merchantSettingsForm.setFields([
                    { name: "upgradeSum", value: res.upgradeSum },
                    {
                        name: "commissionPercentage",
                        value: res.commissionPercentage,
                    },
                ]);
            });
    }, [dispatch, merchantSettingsForm]);

    const onSubmit = ({ upgradeSum, commissionPercentage }) => {
        dispatch(updateMerchantSettings({ upgradeSum, commissionPercentage }))
            .unwrap()
            .then(() => {
                notification.success({ message: "Setting updated" });
            })
            .catch((err) => {
                err = JSON.parse(err);

                if (err.status === 422) {
                    notification.error({ message: err.data.message });

                    const errors = err.data.errors;
                    for (const error in errors) {
                        merchantSettingsForm.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else {
                    notification.error({ message: err.data.message });
                }
            });
    };

    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card
                        loading={merchantSetting.loading}
                        bordered
                        style={{ borderRadius: "10px" }}
                    >
                        {!merchantSetting.loading && (
                            <>
                                <Title
                                    level={5}
                                    style={{
                                        textAlign: "center",
                                        color: grey[4],
                                    }}
                                >
                                    Merchant Settings
                                </Title>
                                <Form
                                    form={merchantSettingsForm}
                                    layout="vertical"
                                    onFinish={onSubmit}
                                >
                                    <Item
                                        label="Users pay"
                                        name="upgradeSum"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field required",
                                            },
                                        ]}
                                        initialValue={
                                            merchantSetting.upgradeSum
                                        }
                                    >
                                        <Input
                                            addonBefore="the sum of"
                                            addonAfter="to upgrade to merchant"
                                        />
                                    </Item>
                                    <Item
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field required",
                                            },
                                        ]}
                                        label="Merchants get"
                                        name="commissionPercentage"
                                        initialValue={
                                            merchantSetting.commissionPercentage
                                        }
                                    >
                                        <Input addonAfter="% as commission" />
                                    </Item>
                                    <Item>
                                        <Button
                                            type="primary"
                                            style={{
                                                backgroundColor: orange[4],
                                                border: 0,
                                            }}
                                            htmlType="submit"
                                        >
                                            Update settings
                                        </Button>
                                    </Item>
                                </Form>
                            </>
                        )}
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default MerchantSettings;
