import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { Typography, Table, Row, Col, Modal, Button } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { green } from "@ant-design/colors";
import moment from "moment";
import { getAllTransactions } from "../../features/transactions/transactionSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

function Transactions() {
    usePageTitle("Transactions");
    const dispatch = useDispatch();
    const style = useSelector((state) => state.style);
    const loading = useSelector((state) => state.transaction.loading);
    const [transactions, setTransactions] = useState([]);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 1,
        total: 0,
        position: ["bottomRight"],
    });

    const handleTableChange = ({ current, pageSize }) => {
        dispatch(getAllTransactions({ page: current, perPage: pageSize }))
            .unwrap()
            .then((res) => {
                setTransactions(res.data);
                setPagination({
                    ...pagination,
                    current: res.current_page,
                    pageSize: res.per_page,
                    total: res.total,
                });
            });
    };
    useEffect(() => {
        (() => {
            dispatch(getAllTransactions({ page: 1, perPage: 20 }))
                .unwrap()
                .then((res) => {
                    setTransactions(res.data);
                    setPagination({
                        current: res.current_page,
                        pageSize: res.per_page,
                        total: res.total,
                        position: ["bottomRight"],
                    });
                });
        })();
    }, [dispatch]);

    const [selectedTransaction, setSelectedTransaction] = useState(null);
    const [isModalVisble, setIsModalVisible] = useState(false);

    const handleSelectTransaction = (transaction) => {
        setSelectedTransaction(transaction);
        showModal();
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setSelectedTransaction(null);
        setIsModalVisible(false);
    };

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            align: "center",
            render: (_, transaction) => <>{transaction.user.name}</>,
        },
        {
            title: "Customer Number",
            dataIndex: "customer",
            align: "center",
        },
        {
            title: "Amount",
            dataIndex: "amount",
            align: "center",
        },
        {
            title: "Reference",
            dataIndex: "reference",
            align: "center",
        },
        {
            title: "Time of purchase",
            dataIndex: "time_of_purchase",
            align: "center",
            render: (top) => moment(top).format("Y-m-d H:m:s"),
        },
        {
            title: "Action",
            dataIndex: "action",
            align: "center",
            render: (_, transaction) => {
                return (
                    <>
                        <Button
                            size="small"
                            style={{
                                backgroundColor: green[5],
                                border: 0,
                                color: "honeydew",
                                marginBottom: 4,
                            }}
                            onClick={() => handleSelectTransaction(transaction)}
                        >
                            View
                        </Button>
                    </>
                );
            },
        },
    ];
    return (
        <AuthLayout>
            <Title
                style={{ color: style.secondaryColor, marginLeft: 5 }}
                level={4}
            >
                View All Transactions
            </Title>

            <div style={{ padding: 5 }}>
                <div style={{ overflowX: "auto" }}>
                    <Table
                        size="small"
                        pagination={pagination}
                        bordered
                        columns={columns}
                        dataSource={transactions}
                        onChange={handleTableChange}
                        rowKey={(t) => t.id}
                        loading={loading}
                    />
                </div>
            </div>
            <Modal
                title="Transaction Details"
                visible={isModalVisble}
                onOk={handleOk}
                onCancel={handleOk}
            >
                <Row gutter={8}>
                    <Col xs={24} md={8}>
                        <b>Customer</b>: {selectedTransaction?.customer}
                    </Col>
                    <Col xs={24} md={8}>
                        <b>Email</b>: {selectedTransaction?.amount}
                    </Col>
                    <Col xs={24} md={8}>
                        {/* <b>Phone number</b>: {selectedTransaction?.phone} */}
                    </Col>
                </Row>
            </Modal>
        </AuthLayout>
    );
}

export default Transactions;
