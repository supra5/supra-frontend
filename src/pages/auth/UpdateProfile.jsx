import { blue, grey } from "@ant-design/colors";
import {
    Card,
    Col,
    Row,
    Typography,
    Form,
    Input,
    Button,
    notification,
    Space,
} from "antd";
import React from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { useDispatch, useSelector } from "react-redux";
import {
    updateProfile,
    changePassword,
    deleteSavedCard,
} from "../../features/auth/authSlice";
import phone from "phone";
import { usePageTitle } from "../../hooks/usePageTitle";
import { DeleteOutlined } from "@ant-design/icons";

const { Title, Text } = Typography;

const { Item } = Form;

function UpdateProfile() {
    usePageTitle("Update Profile");
    const [profileForm] = Form.useForm();
    const [changePasswordForm] = Form.useForm();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.auth.loading);
    const user = useSelector((state) => state.auth.user);

    const onDeleteCard = () => {
        if (window.confirm("Are you sure?")) {
            dispatch(deleteSavedCard())
                .unwrap()
                .then(() => {
                    notification.info({ message: "Card Deleted" });
                })
                .catch((err) => {
                    err = JSON.parse(err);
                    if (err) {
                        notification.error({ message: err.data.message });
                    } else {
                        notification.error({ message: "Server Error" });
                    }
                });
        }
    };

    const onUpdateProfile = ({ name, phone, email }) => {
        dispatch(updateProfile({ name, phone, email }))
            .unwrap()
            .then(() => {
                notification.success({ message: "Profile Updated" });
                profileForm.resetFields();
            })
            .catch((err) => {
                err = JSON.parse(err);
                if (err.status === 422) {
                    notification.error({ message: err.data.message });
                    const errors = err.data.errors;
                    for (const error in errors) {
                        profileForm.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else {
                    notification.error({
                        message: "Server error. Please try again later!",
                    });
                }
            });
    };
    const onChangePassword = ({
        oldPassword,
        newPassword,
        newPassword_confirmation,
    }) => {
        dispatch(
            changePassword({
                oldPassword,
                newPassword,
                newPassword_confirmation,
            })
        )
            .unwrap()
            .then(() => {
                notification.success({
                    message: "Password updated succesfully",
                });
                changePasswordForm.resetFields();
            })
            .catch((err) => {
                err = JSON.parse(err);
                if (err.status === 422) {
                    notification.error({ message: err.data.message });
                    const errors = err.data.errors;
                    for (const error in errors) {
                        changePasswordForm.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else {
                    notification.error({
                        message: "Server error. Please try again later!",
                    });
                }
            });
    };
    return (
        <AuthLayout>
            <Space direction="vertical" style={{ width: "100%" }}>
                {user.hasCard && (
                    <Row justify="center">
                        <Col xs={24} md={12}>
                            <Card style={{ backgroundColor: blue[0] }}>
                                <Row justify="space-between">
                                    <Col>
                                        <Text
                                            level={5}
                                            style={{
                                                textAlign: "center",
                                                color: grey[5],
                                            }}
                                        >
                                            Delete your saved card that ends
                                            with:
                                        </Text>
                                        <Title level={5}>
                                            {user.card_last_digits}
                                        </Title>
                                    </Col>
                                    <Col>
                                        <Button
                                            onClick={onDeleteCard}
                                            size="large"
                                            shape="circle"
                                        >
                                            <DeleteOutlined />
                                        </Button>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                )}

                <Row justify="center">
                    <Col xs={24} md={12}>
                        <Card size="small">
                            <Title
                                level={5}
                                style={{ textAlign: "center", color: grey[5] }}
                            >
                                Update Profile
                            </Title>
                            <Form
                                onFinish={onUpdateProfile}
                                layout="vertical"
                                form={profileForm}
                                initialValues={{
                                    name: user.name,
                                    email: user.email,
                                    phone: user.phone,
                                }}
                            >
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message: "You name is required!",
                                        },
                                    ]}
                                    name="name"
                                    label="Name"
                                >
                                    <Input />
                                </Item>
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message: "You email is required!",
                                        },
                                        {
                                            type: "email",
                                            message: "Must be a valid email",
                                        },
                                    ]}
                                    name="email"
                                    label="E-mail"
                                >
                                    <Input />
                                </Item>
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message: "Your phone is required!",
                                        },
                                        {
                                            validator: (_, value) => {
                                                if (
                                                    !value ||
                                                    phone(value, {
                                                        country: "NG",
                                                    }).isValid
                                                ) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(
                                                    "Must be a valid phone number"
                                                );
                                            },
                                        },
                                    ]}
                                    name="phone"
                                    label="Phone"
                                >
                                    <Input />
                                </Item>
                                <Item>
                                    <Button
                                        loading={loading}
                                        htmlType="submit"
                                        style={{
                                            border: 0,
                                            color: "white",
                                            backgroundColor: "#8e104b",
                                        }}
                                    >
                                        Update Profile
                                    </Button>
                                </Item>
                            </Form>
                        </Card>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col xs={24} md={12}>
                        <Card>
                            <Title
                                style={{ textAlign: "center", color: grey[5] }}
                                level={5}
                            >
                                Change Password
                            </Title>
                            <Form
                                onFinish={onChangePassword}
                                layout="vertical"
                                form={changePasswordForm}
                            >
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message:
                                                "You must provide your old password!",
                                        },
                                    ]}
                                    name="oldPassword"
                                    label="Old Password"
                                >
                                    <Input.Password />
                                </Item>
                                <Item
                                    rules={[
                                        {
                                            required: true,
                                            message: "Field required",
                                        },
                                        {
                                            min: 8,
                                            message:
                                                "Mininum of 8 characters required",
                                        },
                                    ]}
                                    name="newPassword"
                                    label="New Password"
                                >
                                    <Input.Password></Input.Password>
                                </Item>
                                <Item
                                    dependencies={["newPassword"]}
                                    rules={[
                                        ({ getFieldValue }) => ({
                                            validator: (_, value) => {
                                                if (
                                                    !value ||
                                                    getFieldValue(
                                                        "newPassword"
                                                    ) === value
                                                ) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(
                                                    "Must be equal to the new password!"
                                                );
                                            },
                                        }),
                                    ]}
                                    name="newPassword_confirmation"
                                    label="Confim New Password"
                                >
                                    <Input.Password></Input.Password>
                                </Item>
                                <Item>
                                    <Button
                                        loading={loading}
                                        style={{
                                            border: 0,
                                            color: "white",
                                            backgroundColor: "#8e104b",
                                        }}
                                        htmlType="submit"
                                    >
                                        Change Password
                                    </Button>
                                </Item>
                            </Form>
                        </Card>
                    </Col>
                </Row>
            </Space>
        </AuthLayout>
    );
}

export default UpdateProfile;
