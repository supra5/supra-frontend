import {
    Card,
    Row,
    Typography,
    Col,
    Form,
    Select,
    Button,
    InputNumber,
    Input,
    notification,
    Space,
    Switch,
} from "antd";

import React, { useEffect, useState } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import $api from "../../services/api";
import phone from "phone";
import { useHistory } from "react-router-dom";
import { grey } from "@ant-design/colors";
import BuyBulkAirtime from "../merchant/BuyBulkAirtime";
import { useSelector } from "react-redux";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Item } = Form;
const { Option } = Select;
const { Title } = Typography;

function BuyAirtime() {
    usePageTitle("Buy Airtime");
    const merchantLink = useSelector((state) => state.marketing.merchantLink);
    const user = useSelector((state) => state.auth.user);
    const history = useHistory();
    const [airtimeForm] = Form.useForm();

    const [loading, setLoading] = useState(false);
    const [billers, setBillers] = useState([]);
    useEffect(() => {
        $api.get("/billers/airtime").then((res) => {
            setBillers(res.data.billers);
        });
    }, []);
    const onSubmit = ({ amount, phone_number, network, saveCard, useCard }) => {
        setLoading(true);
        $api.post(
            "/buy-airtime",
            { amount, phone_number, network, saveCard, useCard },
            { params: { link: merchantLink } }
        )
            .then((res) => {
                if (user.hasCard && !!useCard) {
                    notification.success({
                        message: "Successfully bought airtime",
                        duration: 1,
                        onClose: () => {
                            history.push("/my-purchases");
                        },
                    });
                } else {
                    notification.info({
                        message: "Redirecting",
                        duration: 1,
                        onClose: () => {
                            document.location = res.data.data.link;
                        },
                    });
                }
            })
            .catch((err) => {
                if (err.response && err.response.status === 422) {
                    notification.error({ message: err.response.data.message });
                    const errors = err.response.data.errors;
                    for (const error in errors) {
                        airtimeForm.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else if (err.response && err.response.status === 400) {
                    notification.error({
                        message: err.response.data.message,

                        onClose: () => {},
                    });
                } else {
                    notification.error({ message: "Server error" });
                }
            })
            .finally(() => {
                setLoading(false);
            });
    };
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Space style={{ width: "100%" }} direction="vertical">
                        <Card bordered style={{ borderRadius: "10px" }}>
                            <Title
                                level={5}
                                style={{ textAlign: "center", color: grey[4] }}
                            >
                                Select network and enter amount
                            </Title>
                            <Form
                                form={airtimeForm}
                                layout="vertical"
                                onFinish={onSubmit}
                            >
                                <Item
                                    name="network"
                                    label="Network"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Network is required",
                                        },
                                    ]}
                                >
                                    <Select
                                        size="large"
                                        placeholder="Select Network"
                                    >
                                        {billers.map((biller) => (
                                            <Option
                                                key={biller.id}
                                                value={biller.biller_code}
                                            >
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={biller.image}
                                                    alt="logo"
                                                />
                                                {biller.short_name}
                                            </Option>
                                        ))}
                                    </Select>
                                </Item>
                                <Item
                                    name="phone_number"
                                    label="Phone Number"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Phone number is required",
                                        },
                                        {
                                            validator: (_, value) => {
                                                if (
                                                    !value ||
                                                    phone(value, {
                                                        country: "NG",
                                                    }).isValid
                                                ) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(
                                                    new Error(
                                                        "Invalid phone number"
                                                    )
                                                );
                                            },
                                        },
                                    ]}
                                >
                                    <Input placeholder="Enter phone number" />
                                </Item>
                                <Item
                                    label="Amount"
                                    rules={[
                                        {
                                            required: true,
                                            message: "Amount is required",
                                        },
                                        {
                                            type: "integer",
                                            message: "Must be a number",
                                        },
                                    ]}
                                    name="amount"
                                >
                                    <InputNumber
                                        placeholder="N100, N200"
                                        min={100}
                                        size="large"
                                        style={{ width: "100%" }}
                                    />
                                </Item>
                                {user.hasCard ? (
                                    <Item
                                        name="useCard"
                                        label="Use Saved Card"
                                        valuePropName="checked"
                                    >
                                        <Switch />
                                    </Item>
                                ) : (
                                    <Item
                                        name="saveCard"
                                        label="Save Card for future use"
                                        valuePropName="checked"
                                    >
                                        <Switch />
                                    </Item>
                                )}

                                <Item>
                                    <Button
                                        loading={loading}
                                        block
                                        type="primary"
                                        style={{
                                            backgroundColor: "#b1145e",
                                            borderWidth: "0",
                                        }}
                                        size="large"
                                        htmlType="submit"
                                    >
                                        Buy Airtime
                                    </Button>
                                </Item>
                            </Form>
                        </Card>
                        <BuyBulkAirtime />
                    </Space>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BuyAirtime;
