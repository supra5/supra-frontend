import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Form,
    Card,
    Typography,
    Row,
    Col,
    Select,
    Input,
    Button,
    notification,
    Switch,
} from "antd";
import $api from "../../services/api";
import dstvLogo from "../../images/logos/DSTV-logo.svg";
import starTimesLogo from "../../images/logos/StarTimes-logo.png";
import gotvLogo from "../../images/logos/GOTV-logo.svg";
import PageLoading from "../../components/PageLoading";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;
const { Item } = Form;
const { Option } = Select;
function BuyCable() {
    usePageTitle("Buy Cable");
    const history = useHistory();
    const user = useSelector((state) => state.auth.user);
    const [cableForm] = Form.useForm();
    const [billers, setBillers] = useState([]);
    const [currentBillers, setCurrentBillers] = useState([]);
    const changeBillers = (billerCode) => {
        const currentBillers = billers
            .slice()
            .filter((biller) => biller.biller_code === billerCode);
        setCurrentBillers(currentBillers);
    };
    const changeCurrentPlan = (billerName) => {
        const cp = currentBillers.find(
            (biller) => biller.biller_name === billerName
        );
        setCurrentPlan(cp);
        cableForm.setFields([{ name: "amount", value: cp.amount }]);
    };
    const [currentPlan, setCurrentPlan] = useState(null);
    const [loading, setLoading] = useState(false);
    const [billersLoading, setBillersLoading] = useState(false);

    useEffect(() => {
        setBillersLoading(true);
        $api.get("/billers/cable")
            .then((res) => {
                setBillers(res.data.billers);
            })
            .catch(() => {
                notification.error({ message: "Error fetching billers" });
            })
            .finally(() => {
                setBillersLoading(false);
            });
    }, []);
    const onSubmit = ({
        biller_name,
        amount,
        smartcard_number,
        saveCard,
        useCard,
    }) => {
        setLoading(true);
        $api.post("/buy-cable", {
            biller_name,
            amount,
            smartcard_number,
            saveCard,
            useCard,
        })
            .then((res) => {
                if (user.hasCard && useCard) {
                    notification.success({
                        message: "Successfully bought cable plan",
                        duration: 1,
                        onClose: () => {
                            history.push("/my-purchases");
                        },
                    });
                } else {
                    notification.info({
                        message: "Redirecting",
                        duration: 1,
                        onClose: () => {
                            document.location = res.data.data.link;
                        },
                    });
                }
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 422) {
                        notification.error({
                            message: err.response.data.message,
                        });
                        const errors = err.response.data.errors;
                        for (const error in errors) {
                            cableForm.setFields([
                                {
                                    name: error,
                                    errors: errors[error],
                                },
                            ]);
                        }
                    } else if (err.response.status === 400) {
                        notification.error({
                            message: err.response.data.message,
                        });
                    } else {
                        notification.error({ message: "Server error" });
                    }
                } else {
                    notification.error({ message: "Server error" });
                }
            })
            .finally(() => {
                setLoading(false);
            });
    };
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card bordered style={{ borderRadius: "10px" }}>
                        {!billersLoading ? (
                            <>
                                <Title
                                    level={5}
                                    style={{ textAlign: "center" }}
                                >
                                    Select Cable Network and Plan
                                </Title>

                                <Form
                                    form={cableForm}
                                    layout="vertical"
                                    onFinish={onSubmit}
                                >
                                    <Item
                                        name="cable_network"
                                        label="Cable Network"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Network is required",
                                            },
                                        ]}
                                    >
                                        <Select
                                            onChange={(value) => {
                                                changeBillers(value);
                                            }}
                                            placeholder="Select plan"
                                        >
                                            <Option value="BIL121">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        width: "50px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={dstvLogo}
                                                    alt="logo"
                                                />
                                                DSTV
                                            </Option>

                                            <Option value="BIL122">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        width: "50px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={gotvLogo}
                                                    alt="logo"
                                                />
                                                GOTV
                                            </Option>
                                            <Option value="BIL123">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        width: "100px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={starTimesLogo}
                                                    alt="logo"
                                                />
                                                Startimes
                                            </Option>
                                        </Select>
                                    </Item>
                                    <Item
                                        label="Select plan"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field required",
                                            },
                                        ]}
                                        name="biller_name"
                                    >
                                        <Select
                                            size="large"
                                            placeholder="Select Data Plan"
                                            onChange={(value) => {
                                                changeCurrentPlan(value);
                                            }}
                                        >
                                            {currentBillers.map((biller) => (
                                                <Option
                                                    key={biller.id}
                                                    value={biller.biller_name}
                                                >
                                                    {biller.biller_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Item>
                                    <Item
                                        name="smartcard_number"
                                        label="Smartcard Number"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "The smartcard number is required",
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Enter smartcard number" />
                                    </Item>

                                    <Item
                                        hidden
                                        name="amount"
                                        initialValue={currentPlan?.amount}
                                    >
                                        <Input type="hidden" />
                                    </Item>
                                    {user.hasCard ? (
                                        <Item
                                            name="useCard"
                                            valuePropName="checked"
                                            label="Use saved card for quick payment"
                                        >
                                            <Switch />
                                        </Item>
                                    ) : (
                                        <Item
                                            name="saveCard"
                                            valuePropName="checked"
                                            label="Save card for future use"
                                        >
                                            <Switch />
                                        </Item>
                                    )}
                                    <Item>
                                        <Button
                                            loading={loading}
                                            block
                                            type="primary"
                                            style={{
                                                backgroundColor: "#b1145e",
                                                borderWidth: "0",
                                            }}
                                            size="large"
                                            htmlType="submit"
                                        >
                                            Buy Data &nbsp;{" "}
                                            {currentPlan && (
                                                <>
                                                    {" "}
                                                    - &#8358;
                                                    {currentPlan.amount}
                                                </>
                                            )}
                                        </Button>
                                    </Item>
                                </Form>
                            </>
                        ) : (
                            <PageLoading />
                        )}
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BuyCable;
