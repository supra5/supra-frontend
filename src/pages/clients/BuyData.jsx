import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Form,
    Card,
    Typography,
    Row,
    Col,
    Select,
    Input,
    Button,
    notification,
    Switch,
} from "antd";
import phone from "phone";
import $api from "../../services/api";
import mtnLogo from "../../images/logos/MTN-Logo.svg";
import airtelLogo from "../../images/logos/Airtel-Nigeria-Logo.svg";
import gloLogo from "../../images/logos/Globacom-Logo.svg";
import ninemobileLogo from "../../images/logos/9mobile-Logo.svg";
import PageLoading from "../../components/PageLoading";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;
const { Item } = Form;
const { Option } = Select;
function BuyData() {
    usePageTitle("Buy Data");
    const user = useSelector((state) => state.auth.user);
    const history = useHistory();
    const [dataForm] = Form.useForm();
    const [billers, setBillers] = useState([]);
    const [currentBillers, setCurrentBillers] = useState([]);
    const changeBillers = (billerCode) => {
        const currentBillers = billers
            .slice()
            .filter((biller) => biller.biller_code === billerCode);
        setCurrentBillers(currentBillers);
    };
    const changeCurrentPlan = (billerName) => {
        const cp = currentBillers.find(
            (biller) => biller.biller_name === billerName
        );
        setCurrentPlan(cp);
        dataForm.setFields([{ name: "amount", value: cp.amount }]);
    };
    const [currentPlan, setCurrentPlan] = useState(null);
    const [loading, setLoading] = useState(false);
    const [billersLoading, setBillersLoading] = useState(false);

    useEffect(() => {
        setBillersLoading(true);
        $api.get("/billers/data")
            .then((res) => {
                setBillers(res.data.billers);
            })
            .catch(() => {
                notification.error({ message: "Error fetching billers" });
            })
            .finally(() => {
                setBillersLoading(false);
            });
    }, []);
    const onSubmit = ({
        biller_name,
        amount,
        phone_number,
        useCard,
        saveCard,
    }) => {
        setLoading(true);
        $api.post("/buy-data", {
            biller_name,
            amount,
            phone_number,
            useCard,
            saveCard,
        })
            .then((res) => {
                if (user.hasCard && useCard) {
                    notification.success({
                        message: "Successfully bought data plan",
                        duration: 1,
                        onClose: () => {
                            history.push("/my-purchases");
                        },
                    });
                } else {
                    notification.info({
                        message: "Redirecting",
                        duration: 1,
                        onClose: () => {
                            document.location = res.data.data.link;
                        },
                    });
                }
            })
            .catch((err) => {
                console.log(err.response);
            })
            .finally(() => {
                setLoading(false);
            });
    };
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card bordered style={{ borderRadius: "10px" }}>
                        {!billersLoading ? (
                            <>
                                <Title
                                    level={5}
                                    style={{ textAlign: "center" }}
                                >
                                    Select network and data plan
                                </Title>

                                <Form
                                    form={dataForm}
                                    layout="vertical"
                                    onFinish={onSubmit}
                                >
                                    <Item
                                        name="network"
                                        label="Network"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Network is required",
                                            },
                                        ]}
                                    >
                                        <Select
                                            onChange={(value) => {
                                                changeBillers(value);
                                            }}
                                            placeholder="Select network"
                                        >
                                            <Option value="BIL104">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={mtnLogo}
                                                    alt="logo"
                                                />
                                                MTN
                                            </Option>
                                            <Option value="BIL106">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={airtelLogo}
                                                    alt="logo"
                                                />
                                                Airtel
                                            </Option>
                                            <Option value="BIL105">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={gloLogo}
                                                    alt="logo"
                                                />
                                                Glo
                                            </Option>
                                            <Option value="BIL107">
                                                <img
                                                    style={{
                                                        height: "30px",
                                                        marginRight: "10px",
                                                    }}
                                                    src={ninemobileLogo}
                                                    alt="logo"
                                                />
                                                9mobile
                                            </Option>
                                        </Select>
                                    </Item>
                                    <Item
                                        label="Data plan"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field required",
                                            },
                                        ]}
                                        name="biller_name"
                                    >
                                        <Select
                                            size="large"
                                            placeholder="Select Data Plan"
                                            onChange={(value) => {
                                                changeCurrentPlan(value);
                                            }}
                                        >
                                            {currentBillers.map((biller) => (
                                                <Option
                                                    key={biller.id}
                                                    value={biller.biller_name}
                                                >
                                                    {biller.biller_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Item>
                                    <Item
                                        name="phone_number"
                                        label="Phone Number"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Phone number is required",
                                            },
                                            {
                                                validator: (_, value) => {
                                                    if (
                                                        !value ||
                                                        phone(value, {
                                                            country: "NG",
                                                        }).isValid
                                                    ) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        new Error(
                                                            "Invalid phone number"
                                                        )
                                                    );
                                                },
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Enter phone number" />
                                    </Item>

                                    <Item
                                        hidden
                                        name="amount"
                                        initialValue={currentPlan?.amount}
                                    >
                                        <Input type="hidden" />
                                    </Item>
                                    {user.hasCard ? (
                                        <Item
                                            name="useCard"
                                            valuePropName="checked"
                                            label="Use saved card for quick payment"
                                        >
                                            <Switch />
                                        </Item>
                                    ) : (
                                        <Item
                                            name="saveCard"
                                            valuePropName="checked"
                                            label="Save card for future use"
                                        >
                                            <Switch />
                                        </Item>
                                    )}

                                    <Item>
                                        <Button
                                            loading={loading}
                                            block
                                            type="primary"
                                            style={{
                                                backgroundColor: "#b1145e",
                                                borderWidth: "0",
                                            }}
                                            size="large"
                                            htmlType="submit"
                                        >
                                            Buy Data &nbsp;{" "}
                                            {currentPlan && (
                                                <>
                                                    {" "}
                                                    - &#8358;
                                                    {currentPlan.amount}
                                                </>
                                            )}
                                        </Button>
                                    </Item>
                                </Form>
                            </>
                        ) : (
                            <PageLoading />
                        )}
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BuyData;
