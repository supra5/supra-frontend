import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Form,
    Card,
    Typography,
    Row,
    Col,
    Select,
    Input,
    Button,
    notification,
    InputNumber,
    Switch,
} from "antd";
import $api from "../../services/api";
import PageLoading from "../../components/PageLoading";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;
const { Item } = Form;
const { Option } = Select;
function BuyPower() {
    usePageTitle("Buy Power");
    const [cableForm] = Form.useForm();
    const [billers, setBillers] = useState([]);
    const history = useHistory();
    const user = useSelector((state) => state.auth.user);
    const [loading, setLoading] = useState(false);
    const [billersLoading, setBillersLoading] = useState(false);

    useEffect(() => {
        setBillersLoading(true);
        $api.get("/billers/power")
            .then((res) => {
                setBillers(res.data.billers);
            })
            .catch(() => {
                notification.error({ message: "Error fetching billers" });
            })
            .finally(() => {
                setBillersLoading(false);
            });
    }, []);
    const onSubmit = ({
        biller_name,
        amount,
        meter_number,
        useCard,
        saveCard,
    }) => {
        setLoading(true);
        $api.post("/buy-power", {
            biller_name,
            amount,
            meter_number,
            saveCard,
            useCard,
        })
            .then((res) => {
                if (
                    user.hasCard &&
                    cableForm.getFieldValue("useCard") === true
                ) {
                    notification.success({
                        message: "Successfully bought power plan.",
                        duration: 1,
                        onClose: () => {
                            history.push("/my-purchases?status=success");
                        },
                    });
                } else {
                    notification.info({
                        message: "Redirecting",
                        duration: 1,
                        onClose: () => {
                            document.location = res.data.data.link;
                        },
                    });
                }
            })
            .catch((err) => {
                if (err.response.status === 422) {
                    notification.error({ message: err.response.data.message });
                    const errors = err.response.data.errors;
                    for (const error in errors) {
                        cableForm.setFields([
                            {
                                name: error,
                                errors: errors[error],
                            },
                        ]);
                    }
                } else {
                    notification.error({
                        message: "There was an error processing your request",
                        description: "Please try again later",
                    });
                }

                setLoading(false);
            });
    };
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card bordered style={{ borderRadius: "10px" }}>
                        {!billersLoading ? (
                            <>
                                <Title
                                    level={5}
                                    style={{ textAlign: "center" }}
                                >
                                    Select Power Company and Plan
                                </Title>

                                <Form
                                    form={cableForm}
                                    layout="vertical"
                                    onFinish={onSubmit}
                                >
                                    <Item
                                        label="Select plan"
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field required",
                                            },
                                        ]}
                                        name="biller_name"
                                    >
                                        <Select
                                            size="large"
                                            placeholder="Select Data Plan"
                                        >
                                            {billers.map((biller) => (
                                                <Option
                                                    key={biller.id}
                                                    value={biller.biller_name}
                                                >
                                                    {biller.biller_name}
                                                </Option>
                                            ))}
                                        </Select>
                                    </Item>
                                    <Item
                                        name="meter_number"
                                        label="Meter Number"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "The meter number is required",
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                            placeholder="Enter meter number"
                                        />
                                    </Item>

                                    <Item
                                        name="amount"
                                        label="Amount"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "The amount is required",
                                            },
                                        ]}
                                    >
                                        <InputNumber
                                            size="large"
                                            style={{ width: "100%" }}
                                            placeholder="Enter Amount"
                                        />
                                    </Item>
                                    {user.hasCard ? (
                                        <Item
                                            name="useCard"
                                            label="Use Saved Card"
                                            valuePropName="checked"
                                        >
                                            <Switch defaultChecked />
                                        </Item>
                                    ) : (
                                        <Item
                                            name="saveCard"
                                            label="Save Card for future use"
                                            valuePropName="checked"
                                        >
                                            <Switch />
                                        </Item>
                                    )}
                                    <Item>
                                        <Button
                                            loading={loading}
                                            block
                                            type="primary"
                                            style={{
                                                backgroundColor: "#b1145e",
                                                borderWidth: "0",
                                            }}
                                            size="large"
                                            htmlType="submit"
                                        >
                                            Buy Power Subscription
                                        </Button>
                                    </Item>
                                </Form>
                            </>
                        ) : (
                            <PageLoading />
                        )}
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BuyPower;
