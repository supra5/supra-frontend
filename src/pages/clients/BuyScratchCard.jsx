import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
    Row,
    Col,
    Card,
    Typography,
    Form,
    Select,
    Button,
    notification,
    InputNumber,
    Switch,
} from "antd";
import { grey } from "@ant-design/colors";
import $api from "../../services/api";
import { v4 as uuid } from "uuid";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Item } = Form;
const { Title } = Typography;
const { Option } = Select;

function BuyScratchCard() {
    usePageTitle("Buy Scratch Card");
    const history = useHistory();
    const user = useSelector((state) => state.auth.user);
    const merchantLink = useSelector((state) => state.marketing.merchantLink);
    const [billersLoading, setBillersLoading] = useState(false);
    const [waecBillers, setWaecBillers] = useState([]);
    const [selectedAmount, setSelectedAmount] = useState(0);
    const [selectedDescription, setSelectedDescription] = useState(null);
    const [numberOfPins, setNumberOfPins] = useState(0);
    const [form] = Form.useForm();

    const onSubmit = ({ pinValue, numberOfPins, useCard, saveCard }) => {
        setBillersLoading(true);
        $api.post(
            "/buy-waec",
            {
                pinValue,
                numberOfPins,
                useCard,
                saveCard,
                pinDescription: selectedDescription,
            },
            { params: { link: merchantLink } }
        )
            .then((res) => {
                if (user.hasCard && useCard) {
                    notification.success({
                        message: "Successfully bought scratchcard",
                        duration: 1,
                        onClose: () => {
                            history.push("/my-purchases");
                        },
                    });
                } else {
                    notification.info({
                        message: "Redirecting",
                        duration: 1,
                        onClose: () => {
                            document.location = res.data.data.link;
                        },
                    });
                }
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 422) {
                        notification.error({
                            message: err.response.data.message,
                        });
                        const errors = err.response.data.errors;

                        for (const error in errors) {
                            form.setFields([
                                { name: error, errors: errors[error] },
                            ]);
                        }
                    } else if (err.response.status === 400) {
                        notification.error({
                            message: err.response.data.message,
                        });
                    } else if (err.response.status === 500) {
                        notification.error({ message: "Server error" });
                    }
                } else {
                    notification.error({
                        message: "You are offline. Try when you are connected",
                    });
                }
            })
            .finally(() => {
                setBillersLoading(false);
            });
    };

    useEffect(() => {
        setBillersLoading(true);
        $api.get("/billers/waec")
            .then((res) => {
                setWaecBillers(res.data.billers);
            })
            .catch(() => {
                notification.error({ message: "Error fetching billers" });
            })
            .finally(() => {
                setBillersLoading(false);
            });
    }, []);

    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card bordered style={{ borderRadius: "10px" }}>
                        <Title
                            level={5}
                            style={{ color: grey[5], textAlign: "center" }}
                        >
                            Buy WAEC Scratchcard
                        </Title>
                        <Form
                            onFinish={onSubmit}
                            onFieldsChange={(fields, allFields) => {
                                const billerValue = allFields[0].value || 0;
                                const numberOfPinsValue =
                                    allFields[1].value || 0;
                                setSelectedAmount(billerValue);
                                setNumberOfPins(numberOfPinsValue);
                                setSelectedDescription(
                                    waecBillers[
                                        waecBillers.findIndex(
                                            (biller) =>
                                                biller.amount === billerValue
                                        )
                                    ].description
                                );
                            }}
                            layout="vertical"
                            size="large"
                            form={form}
                        >
                            <Item name="pinValue" label="Select Option">
                                <Select placeholder="Select WAEC Biller">
                                    {waecBillers.map((biller) => (
                                        <Option
                                            key={uuid()}
                                            value={biller.amount}
                                        >
                                            {biller.description}
                                        </Option>
                                    ))}
                                </Select>
                            </Item>
                            <Item
                                initialValue={1}
                                name="numberOfPins"
                                label="Number of pins"
                                rules={[
                                    {
                                        required: true,
                                        message: "Field required",
                                    },
                                ]}
                            >
                                <InputNumber style={{ width: "100%" }} />
                            </Item>
                            {user.hasCard ? (
                                <Item
                                    name="useCard"
                                    label="Use Saved Card"
                                    valuePropName="checked"
                                >
                                    <Switch />
                                </Item>
                            ) : (
                                <Item
                                    name="saveCard"
                                    label="Save Card for future use"
                                    valuePropName="checked"
                                >
                                    <Switch />
                                </Item>
                            )}
                            <Item>
                                <Button
                                    style={{
                                        backgroundColor: "rgb(177, 20, 94)",
                                        color: "honeydew",
                                        border: 0,
                                    }}
                                    htmlType="submit"
                                    block
                                    loading={billersLoading}
                                >
                                    Pay &#8358;{selectedAmount * numberOfPins}
                                </Button>
                            </Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BuyScratchCard;
