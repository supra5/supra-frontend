import React from "react";
import { Typography, Row, Col } from "antd";
import AuthLayout from "../../layouts/AuthLayout";

import { Link } from "react-router-dom";
import { v4 as uuid } from "uuid";

import { grey, orange, blue, green, purple } from "@ant-design/colors";
import { CardWidget } from "../../components/Widgets";
import {
    PhoneOutlined,
    WifiOutlined,
    ThunderboltFilled,
    GlobalOutlined,
    KeyOutlined,
} from "@ant-design/icons";
import { usePageTitle } from "../../hooks/usePageTitle";
import MyLimitedTransactionTable from "../../components/MyLimitedTransactionTable";

const { Title, Text } = Typography;

const links = [
    {
        name: "Buy Airtime",
        icon: (
            <PhoneOutlined
                style={{
                    fontSize: "1.5em",
                    color: orange[5],
                }}
            />
        ),
        link: "/buy-airtime",
        key: uuid(),
    },
    {
        name: "Buy Data",
        icon: (
            <WifiOutlined
                style={{
                    fontSize: "1.5em",
                    color: blue[5],
                }}
            />
        ),
        link: "/buy-data",
        key: uuid(),
    },
    {
        name: "Buy Cable",
        icon: (
            <GlobalOutlined
                style={{
                    fontSize: "1.5em",
                    color: green[5],
                }}
            />
        ),
        link: "/buy-cable",
        key: uuid(),
    },
    {
        name: "Recharge power subscription",
        link: "/buy-power",
        icon: (
            <ThunderboltFilled
                style={{
                    fontSize: "1.5em",
                    color: purple[5],
                }}
            />
        ),
        key: uuid(),
    },

    {
        name: "Buy WAEC scratch card",
        link: "/buy-waec-scratchcard",
        key: uuid(),
        icon: <KeyOutlined style={{ fontSize: "1.5em", color: purple[5] }} />,
    },
];

const widgetSpacing = {
    xs: 12,
    lg: 4,
    style: { marginBottom: "2em" },
};

const menus = links.map((link, index) => (
    <Col key={link.key} {...widgetSpacing}>
        <Link to={link.link}>
            <CardWidget colorKey={index}>
                <Row gutter={5}>
                    <Col xs={8}>{link.icon}</Col>
                    <Col xs={16}>
                        <Text
                            style={{
                                color: grey[3],
                                textAlign: "center",
                            }}
                        >
                            {link.name}
                        </Text>
                    </Col>
                </Row>
            </CardWidget>
        </Link>
    </Col>
));

function Dashboard() {
    usePageTitle("Dashboard");

    return (
        <AuthLayout>
            <div style={{ padding: "1em" }}>
                <Title level={4}>Dashboard</Title>
                <Row gutter={3}>{menus}</Row>
                <MyLimitedTransactionTable />
            </div>
        </AuthLayout>
    );
}

export default Dashboard;
