import {
    notification,
    Table,
    Typography,
    Card,
    Button,
    Modal,
    Row,
    Col,
} from "antd";
import React, { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import AuthLayout from "../../layouts/AuthLayout";
import { getMyTransactions } from "../../features/transactions/transactionSlice";
import { usePageTitle } from "../../hooks/usePageTitle";
import { blue, grey } from "@ant-design/colors";

const { Title, Text } = Typography;

const pinColumns = [
    {
        title: "Serial Number",
        dataIndex: "serialNumber",
        align: "center",
    },
    {
        title: "PIN",
        dataIndex: "pin",
        align: "center",
    },
    {
        title: "Expires On",
        dataIndex: "expires_on",
        align: "center",
    },
];

function MyPurchases() {
    usePageTitle("My Purchases");
    const location = useLocation();
    const history = useHistory();

    const loading = useSelector((state) => state.transaction.loading);
    const dispatch = useDispatch();
    const [transactions, setTransactions] = useState([]);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 1,
        total: 0,
        position: ["topRight", "bottomRight"],
    });
    const [visible, setVisible] = useState(false);
    const [selectedTransaction, setSelectedTransaction] = useState(null);

    const selectTransaction = (transaction) => {
        if (transaction) {
            setSelectedTransaction(transaction);
            setVisible(true);
        }
    };

    const onCloseModal = () => {
        setVisible(false);
        setSelectedTransaction(null);
    };

    const handleTableChange = ({ current, pageSize }) => {
        dispatch(getMyTransactions({ page: current, perPage: pageSize }))
            .unwrap()
            .then((res) => {
                setTransactions(res.data);
                setPagination({
                    ...pagination,
                    current: res.current_page,
                    pageSize: res.per_page,
                    total: res.total,
                });
            });
    };

    useEffect(() => {
        (() => {
            dispatch(getMyTransactions({ page: 1, perPage: 20 }))
                .unwrap()
                .then((res) => {
                    setTransactions(res.data);
                    setPagination({
                        current: res.current_page,
                        pageSize: res.per_page,
                        total: res.total,
                        position: ["topRight", "bottomRight"],
                    });
                });
        })();
    }, [dispatch]);

    useEffect(() => {
        const status = new URLSearchParams(location.search).get("status");
        const message = new URLSearchParams(location.search).get("message");
        if (status === "success") {
            notification.success({
                message: "Purchase successful",
                onClose: () => {
                    history.replace(location.pathname);
                },
            });
        } else if (status === "cancelled") {
            notification.warning({
                message: "Purchase cancelled",
                onClose: () => {
                    history.replace(location.pathname);
                },
            });
        } else if (status === "error") {
            notification.warning({
                message: "Purchase error",
                description: message,
                onClose: () => {
                    history.replace(location.pathname);
                },
            });
        }
    }, [location, history]);

    const columns = [
        {
            title: <Text style={{ fontWeight: "bold" }}>Name</Text>,
            dataIndex: "biller_name",
            align: "center",
        },
        {
            title: <Text style={{ fontWeight: "bold" }}>Type</Text>,
            dataIndex: "biller_type",
            align: "center",
        },
        {
            title: <Text style={{ fontWeight: "bold" }}>Customer</Text>,
            dataIndex: "customer",
            align: "center",
        },
        {
            title: <Text style={{ fontWeight: "bold" }}>Amount</Text>,
            dataIndex: "amount",
            align: "center",
            render: (amount) => {
                return <>&#8358;{amount}</>;
            },
        },
        {
            title: <Text style={{ fontWeight: "bold" }}>Time Of Purchase</Text>,
            dataIndex: "time_of_purchase",
            align: "center",
        },
        {
            title: <Text style={{ fontWeight: "bold" }}>Action</Text>,
            dataIndex: "action",
            align: "center",
            render: (_, transaction) => {
                let customerKey;
                if (transaction?.biller_type?.toLowerCase() === "power") {
                    customerKey = "Meter Number";
                } else if (
                    transaction?.biller_type?.toLowerCase() === "airtime" ||
                    transaction?.biller_type?.toLowerCase() === "data" ||
                    transaction?.biller_type?.toLowerCase() === "data plan"
                ) {
                    customerKey = "Phone Number";
                } else {
                    customerKey = "Smartcard Number";
                }
                return (
                    <>
                        {selectedTransaction?.id === transaction.id ? (
                            <Modal
                                bodyStyle={{ backgroundColor: blue[0] }}
                                onOk={onCloseModal}
                                onCancel={onCloseModal}
                                visible={visible}
                                title={
                                    <>
                                        <Title
                                            style={{ color: grey[4] }}
                                            level={4}
                                        >
                                            Transaction Detail
                                        </Title>
                                    </>
                                }
                                width="70%"
                                style={{ overflow: "auto" }}
                            >
                                <Title
                                    level={4}
                                    style={{
                                        color: grey[4],
                                        marginBottom: "2rem",
                                    }}
                                >
                                    Transaction reference:
                                    {transaction.reference}
                                </Title>
                                <Row style={{ marginBottom: "1.5em" }}>
                                    <Col xs={12} md={8}>
                                        <Title level={5}>Biller Name</Title>
                                        <Text>{transaction.biller_name}</Text>
                                    </Col>
                                    <Col xs={12} md={8}>
                                        <Title level={5}>Biller Type</Title>
                                        <Text>
                                            {transaction.biller_type?.toUpperCase()}
                                        </Text>
                                    </Col>
                                    {!transaction.pin && (
                                        <Col xs={12} md={8}>
                                            <Title level={5}>
                                                {customerKey}
                                            </Title>
                                            <Text style={{ fontSize: "1.3em" }}>
                                                {transaction.customer}
                                            </Text>
                                        </Col>
                                    )}

                                    <Col xs={12} md={8}>
                                        <Title level={5}>Amount</Title>
                                        <Text style={{ fontSize: "1.3em" }}>
                                            &#8358;{transaction.amount}
                                        </Text>
                                    </Col>
                                </Row>
                                {transaction.biller_type === "waec" &&
                                transaction.pin ? (
                                    <>
                                        <Table
                                            bordered
                                            dataSource={transaction.pin}
                                            columns={pinColumns}
                                            rowKey={(p) => p.serialNumber}
                                        />
                                    </>
                                ) : null}
                            </Modal>
                        ) : null}
                        <Button
                            onClick={() => {
                                selectTransaction(transaction);
                            }}
                            type="primary"
                            size="small"
                            shape="round"
                        >
                            View
                        </Button>
                    </>
                );
            },
        },
    ];

    return (
        <AuthLayout>
            <div style={{ padding: 20 }}>
                <Card
                    style={{
                        borderRadius: "1.5em",
                        backgroundColor: blue[0],
                        overflowX: "auto",
                    }}
                >
                    <Title level={4} style={{ color: blue[4] }}>
                        My Purchases
                    </Title>
                    <Table
                        columns={columns}
                        onChange={handleTableChange}
                        dataSource={transactions}
                        loading={loading}
                        rowKey={(t) => t.id}
                        bordered
                        pagination={pagination}
                    />
                </Card>
            </div>
        </AuthLayout>
    );
}

export default MyPurchases;
