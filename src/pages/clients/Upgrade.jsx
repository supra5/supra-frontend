import React, { useState, useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { Row, Col, Card, Typography, Button, notification } from "antd";
import { useSelector, useDispatch } from "react-redux";
import $api from "../../services/api";
import { blue, orange } from "@ant-design/colors";
import { getMerchantSettings } from "../../features/settings/settingsSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

function Upgrade() {
    usePageTitle("Upgrade to merchant");
    const [loading, setLoading] = useState(false);
    const upgradeSum = useSelector(
        (state) => state.settings.merchantSetting.upgradeSum
    );
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getMerchantSettings());
    }, [dispatch]);
    const initiateUpgrade = () => {
        setLoading(true);
        $api.post("/initiate-upgrade")
            .then((res) => {
                notification.info({
                    message: "Redirecting",
                    duration: 1,
                    onClose: () => {
                        document.location = res.data.data.link;
                    },
                });
            })
            .catch((err) => {
                notification.error({ message: err.response.data.message });
            })
            .finally(() => {
                setLoading(false);
            });
    };
    const color = useSelector((state) => state.style);
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card
                        bordered
                        style={{
                            borderRadius: "10px",
                            backgroundColor: blue[0],
                        }}
                    >
                        <Title
                            level={3}
                            style={{
                                textAlign: "center",
                                color: color.secondaryColor,
                            }}
                        >
                            Pay a sum of &#8358;{upgradeSum} to upgrade to
                            merchant
                        </Title>

                        <Button
                            style={{
                                backgroundColor: orange[4],
                                border: 0,
                                color: "white",
                                fontWeight: "bold",
                            }}
                            shape="round"
                            size={"large"}
                            block
                            loading={loading}
                            onClick={initiateUpgrade}
                        >
                            Pay &#8358;{upgradeSum}
                        </Button>
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default Upgrade;
