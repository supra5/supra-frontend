import React from "react";
import FrontendLayout from "../../layouts/FrontendLayout";
import { Button, Form, Input } from "antd";
import axios from "axios";

export default function AirtimeData() {
  const onSubmit = (values) => {
    axios
      .post("http://10.0.75.1:8086/api/add-campaign", { name: values.name })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.log(err.response));
  };
  return (
    <>
      <FrontendLayout>
        <Form onFinish={onSubmit}>
          <Form.Item name="name">
            <Input />
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">Add</Button>
          </Form.Item>
        </Form>
      </FrontendLayout>
    </>
  );
}
