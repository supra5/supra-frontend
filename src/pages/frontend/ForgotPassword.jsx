import React from "react";
import {
    Col,
    Row,
    Typography,
    Form,
    Input,
    Card,
    Button,
    notification,
} from "antd";
import { grey } from "@ant-design/colors";
import FrontendLayout from "../../layouts/FrontendLayout";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import bgImg from "../../images/supra2.webp";

import { requestPasswordReset } from "../../features/auth/authSlice";
import { usePageTitle } from "../../hooks/usePageTitle";
const { Title } = Typography;
const { Item } = Form;

export default function ForgotPassword() {
    usePageTitle("Forgot Password");
    const [passwordRequestForm] = Form.useForm();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.auth.loading);

    const onRequest = async ({ email }) => {
        try {
            const result = await dispatch(requestPasswordReset({ email }));
            unwrapResult(result);

            // console.log(promiseResult);

            notification.success({ message: "Password link sent" });
            passwordRequestForm.resetFields();
        } catch (error) {
            let err = JSON.parse(error);
            if (err.status === 400) {
                passwordRequestForm.setFields([
                    { name: "email", errors: [err.data.message], value: "" },
                ]);
            } else if (err.status === 422) {
                notification.error({ message: err.data.message });
                const errors = err.data.errors;

                for (const error in errors) {
                    passwordRequestForm.setFields([
                        { name: error, errors: errors[error] },
                    ]);
                }
            } else {
                notification.error({
                    message: "Server error. Please try again",
                });
            }
        }
    };

    return (
        <FrontendLayout>
            <Row>
                <Col xs={0} md={14}>
                    <img
                        src={bgImg}
                        alt="Signup Background"
                        style={{ width: "100%", height: "100%" }}
                    />
                </Col>
                <Col xs={24} md={10} style={{ backgroundColor: "white" }}>
                    <Row
                        justify="center"
                        style={{ height: "100%", paddingTop: "2rem" }}
                    >
                        <Col xs={20}>
                            <Card style={{ borderRadius: "10px" }}>
                                <Title
                                    style={{
                                        color: grey[5],
                                        textAlign: "center",
                                    }}
                                    level={5}
                                >
                                    Reset Password
                                </Title>
                                <Form
                                    layout="vertical"
                                    form={passwordRequestForm}
                                    onFinish={onRequest}
                                >
                                    <Item
                                        name="email"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Email Address
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Email address is required",
                                            },
                                            {
                                                type: "email",
                                                message:
                                                    "Must be a valid email address",
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                            placeholder="email@domain.com"
                                        />
                                    </Item>

                                    <Item>
                                        <Button
                                            loading={loading}
                                            htmlType="submit"
                                            size="large"
                                            block
                                            type="primary"
                                            shape="round"
                                        >
                                            Request Password Reset Link
                                        </Button>
                                    </Item>
                                </Form>
                                <Row justify="space-between">
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/register"
                                    >
                                        No account? Sign up
                                    </Link>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </FrontendLayout>
    );
}
