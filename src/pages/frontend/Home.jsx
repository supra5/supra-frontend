import React, { useState } from "react";
import { Typography, Card, Row, Col, Button, Form, Input } from "antd";
import { yellow, red, green } from "@ant-design/colors";
import { SearchOutlined } from "@ant-design/icons";
import { v4 as uuid } from "uuid";
import supraImg from "../../images/supra2.webp";
import { useDispatch } from "react-redux";
import { selectNetwork as sN } from "../../features/cart/cartSlice";

import FrontendLayout from "../../layouts/FrontendLayout";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

const { Item } = Form;
const { Search } = Input;

const networkBoxes = [
    {
        name: "MTN",
        key: uuid(),
        style: { backgroundColor: yellow[3], color: "black" },
    },
    {
        name: "Airtel",
        key: uuid(),
        style: { backgroundColor: red[4] },
    },
    {
        name: "Glo",
        key: uuid(),
        style: { backgroundColor: green[3] },
    },
    {
        name: "9-mobile",
        key: uuid(),
        style: { backgroundColor: green[8] },
    },
];

export default function Home() {
    usePageTitle("Homepage");
    const [networkSelected, setNetworkSelected] = useState(null);

    const dispatch = useDispatch();
    //   const selectedNetwork = useSelector(({ cart }) => cart.selectedNetwork);

    const selectNetwork = (network) => {
        setNetworkSelected(network);
    };

    return (
        <>
            <FrontendLayout>
                <Row
                    justify="center"
                    style={
                        {
                            // transform: "polygon(0 0,100% 0,calc(100% - 32px) 100%,0 100%)",
                        }
                    }
                >
                    <Col
                        xs={24}
                        md={12}
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            paddingTop: "4.5rem",
                        }}
                    >
                        <div>
                            <Title style={{ textAlign: "center" }}>
                                Welcome to Supra
                            </Title>
                            <p
                                style={{
                                    fontSize: "2.2em",
                                    marginBottom: "4.5rem",
                                    textAlign: "center",
                                }}
                            >
                                Your one stop shop for all your bills
                            </p>

                            <Form layout="inline">
                                <Item name="searchBiller">
                                    <Search
                                        prefix={<SearchOutlined />}
                                        placeholder="Search Biller"
                                        enterButton="Search"
                                        allowClear={true}
                                        size="large"
                                        style={{
                                            width: "100%",
                                            marginLeft: "1rem",
                                            paddingBottom: "3.5em",
                                        }}
                                    />
                                </Item>
                            </Form>
                            <div className="network-containers">
                                <Row>
                                    {networkBoxes.map((networkBox) => (
                                        <Col
                                            key={networkBox.key}
                                            xs={12}
                                            lg={6}
                                        >
                                            {networkSelected?.key ===
                                            networkBox.key ? (
                                                <Card
                                                    style={networkBox.style}
                                                    onClick={() => {
                                                        dispatch(sN("water"));
                                                        selectNetwork(
                                                            networkBox
                                                        );
                                                    }}
                                                    className="network-box selected"
                                                >
                                                    {networkBox.name}
                                                </Card>
                                            ) : (
                                                <Card
                                                    style={networkBox.style}
                                                    onClick={() => {
                                                        dispatch(sN("water"));
                                                        selectNetwork(
                                                            networkBox
                                                        );
                                                    }}
                                                    className="network-box"
                                                >
                                                    {networkBox.name}
                                                </Card>
                                            )}
                                        </Col>
                                    ))}
                                </Row>
                            </div>
                        </div>
                    </Col>
                    <Col
                        xs={24}
                        md={12}
                        style={{ backgroundColor: "honeydew" }}
                    >
                        {networkSelected ? (
                            <>
                                <Row
                                    className="active-network-section"
                                    justify="center"
                                    align="middle"
                                    // style={{ height: "100vh" }}
                                >
                                    <Col xs={24}>
                                        <Title style={{ textAlign: "center" }}>
                                            {networkSelected.name}
                                        </Title>
                                    </Col>
                                    <Col span={20}>
                                        <Form>
                                            <Item
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "The Phone number is required",
                                                    },
                                                ]}
                                                name="phone"
                                            >
                                                <Input
                                                    size="large"
                                                    placeholder="Enter Phone Number"
                                                    style={{ width: "100%" }}
                                                />
                                            </Item>
                                            <Item
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "The Amount is required",
                                                    },
                                                ]}
                                                name="amount"
                                            >
                                                <Input
                                                    size="large"
                                                    placeholder="Enter Amount"
                                                    style={{ width: "100%" }}
                                                />
                                            </Item>
                                            <Item>
                                                <Button
                                                    htmlType="submit"
                                                    block
                                                    size="large"
                                                    type="primary"
                                                >
                                                    Continue
                                                </Button>
                                            </Item>
                                        </Form>
                                    </Col>
                                </Row>
                            </>
                        ) : (
                            <>
                                <img
                                    src={supraImg}
                                    alt=""
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                    }}
                                />
                            </>
                        )}
                    </Col>
                </Row>
            </FrontendLayout>
        </>
    );
}
