import React from "react";
import {
    Col,
    Row,
    Typography,
    Form,
    Input,
    Card,
    Button,
    notification,
} from "antd";
import { grey } from "@ant-design/colors";
import FrontendLayout from "../../layouts/FrontendLayout";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import bgImg from "../../images/supra2.webp";
import { usePageTitle } from "../../hooks/usePageTitle";

import { login } from "../../features/auth/authSlice";
const { Title } = Typography;
const { Item } = Form;

export default function Login() {
    usePageTitle("Login");
    const [loginForm] = Form.useForm();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.auth.loading);

    const onLogin = ({ email, password }) => {
        dispatch(login({ email, password }))
            .unwrap()
            .then((res) => {
                notification.success({
                    message: "Succesfully Logged in",
                    duration: 1,
                });
            })
            .catch((err) => {
                console.log(err);
                loginForm.setFields([
                    {
                        name: "password",
                        errors: [err.message],
                        value: "",
                    },
                ]);
            });
    };

    return (
        <FrontendLayout>
            <Row>
                <Col xs={0} md={15}>
                    <img
                        src={bgImg}
                        alt="Login Background"
                        style={{ width: "100%" }}
                    />
                </Col>
                <Col xs={24} md={9} style={{ backgroundColor: "white" }}>
                    <Row
                        justify="center"
                        style={{ height: "100%", paddingTop: "2rem" }}
                    >
                        <Col xs={20}>
                            <Title
                                style={{
                                    color: grey[4],
                                    textAlign: "center",
                                }}
                                level={4}
                            >
                                Sign In To Purchase Your Bills
                            </Title>
                            <Card style={{ borderRadius: "10px" }}>
                                <Form
                                    layout="vertical"
                                    form={loginForm}
                                    onFinish={onLogin}
                                >
                                    <Item
                                        name="email"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Email Address
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Email address is required",
                                            },
                                            {
                                                type: "email",
                                                message:
                                                    "Must be a valid email address",
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                            placeholder="email@domain.com"
                                        />
                                    </Item>
                                    <Item
                                        name="password"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Password
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter your password",
                                            },
                                        ]}
                                    >
                                        <Input.Password
                                            size="large"
                                            placeholder="***********"
                                        />
                                    </Item>
                                    <Item>
                                        <Button
                                            loading={loading}
                                            htmlType="submit"
                                            size="large"
                                            block
                                            type="primary"
                                            shape="round"
                                        >
                                            Login
                                        </Button>
                                    </Item>
                                </Form>
                                <Row justify="space-between">
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/forgot-password"
                                    >
                                        Forgot Password?
                                    </Link>
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/register"
                                    >
                                        No account? Sign up
                                    </Link>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </FrontendLayout>
    );
}
