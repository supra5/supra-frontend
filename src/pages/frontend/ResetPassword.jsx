import React from "react";
import {
    Col,
    Row,
    Typography,
    Form,
    Input,
    Card,
    Button,
    notification,
} from "antd";
import { grey, purple } from "@ant-design/colors";
import FrontendLayout from "../../layouts/FrontendLayout";
import { Link, useParams, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";

import { resetPassword } from "../../features/auth/authSlice";
import { usePageTitle } from "../../hooks/usePageTitle";
const { Title } = Typography;
const { Item } = Form;

export default function ResetPassword() {
    usePageTitle("Reset Password");
    const [resetPasswordForm] = Form.useForm();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.auth.loading);
    const { token } = useParams();
    const history = useHistory();

    const onResetPassword = async ({
        email,
        password,
        password_confirmation,
    }) => {
        try {
            const result = await dispatch(
                resetPassword({ email, password, token, password_confirmation })
            );
            unwrapResult(result);

            // console.log(promiseResult);
            resetPasswordForm.resetFields();
            notification.success({
                message: "Password reset successfully. You can now login",
                duration: 1,
                onClose: () => {
                    history.replace("/login");
                },
            });
        } catch (error) {
            let err = JSON.parse(error);
            if (err.status === 400) {
                resetPasswordForm.setFields([
                    { name: "email", errors: [err.data.email] },
                ]);
            } else if (err.status === 422) {
                notification.error({ message: err.data.message });
                const errors = err.data.errors;

                for (const error in errors) {
                    resetPasswordForm.setFields([
                        { name: error, errors: errors[error] },
                    ]);
                }
            } else {
                notification.error({
                    message: "Server error. Please try again",
                });
            }
        }
    };

    return (
        <FrontendLayout>
            <Row>
                <Col xs={0} md={12}>
                    <Title>Login Writed Up</Title>
                </Col>
                <Col xs={24} md={12} style={{ backgroundColor: purple[3] }}>
                    <Row
                        justify="center"
                        style={{ height: "100vh", paddingTop: "2rem" }}
                    >
                        <Col xs={20}>
                            <Title
                                style={{
                                    color: "honeydew",
                                    textAlign: "center",
                                }}
                                level={3}
                            >
                                Reset Password
                            </Title>
                            <Card style={{ borderRadius: "10px" }}>
                                <Form
                                    layout="vertical"
                                    form={resetPasswordForm}
                                    onFinish={onResetPassword}
                                >
                                    <Item
                                        name="email"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Email Address
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Email address is required",
                                            },
                                            {
                                                type: "email",
                                                message:
                                                    "Must be a valid email address",
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                            placeholder="email@domain.com"
                                        />
                                    </Item>
                                    <Item
                                        name="password"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Password
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message: "Password is required",
                                            },
                                        ]}
                                    >
                                        <Input.Password
                                            size="large"
                                            placeholder="********"
                                        />
                                    </Item>
                                    <Item
                                        name="password_confirmation"
                                        dependencies={["password"]}
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Confirm Password
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message: "Field is required",
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(_, value) {
                                                    if (
                                                        !value ||
                                                        getFieldValue(
                                                            "password"
                                                        ) === value
                                                    ) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        new Error(
                                                            "Must be equal to password"
                                                        )
                                                    );
                                                },
                                            }),
                                        ]}
                                    >
                                        <Input.Password
                                            size="large"
                                            placeholder="*********"
                                        />
                                    </Item>
                                    <Item>
                                        <Button
                                            loading={loading}
                                            htmlType="submit"
                                            size="large"
                                            block
                                            type="primary"
                                            shape="round"
                                        >
                                            Reset Password
                                        </Button>
                                    </Item>
                                </Form>
                                <Row justify="space-between">
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/register"
                                    >
                                        No account? Sign up
                                    </Link>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </FrontendLayout>
    );
}
