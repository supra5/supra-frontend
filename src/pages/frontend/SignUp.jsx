import React from "react";
import {
    Row,
    Col,
    Typography,
    Card,
    Form,
    Input,
    Button,
    notification,
} from "antd";
import FrontendLayout from "../../layouts/FrontendLayout";
import { Link } from "react-router-dom";
import { grey } from "@ant-design/colors";
import { useSelector, useDispatch } from "react-redux";
import phone from "phone";
import { signUpUser } from "../../features/auth/authSlice";
import bgImg from "../../images/supra2.webp";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;
const { Item } = Form;
export default function SignUp() {
    usePageTitle("Sign Up");
    const dispatch = useDispatch();
    const onSignup = ({
        email,
        password,
        password_confirmation,
        name,
        phone,
    }) => {
        dispatch(
            signUpUser({ email, password, password_confirmation, name, phone })
        )
            .unwrap()
            .then((res) => {
                notification.success({
                    message: "User registered successfully",
                });
            })
            .catch((err) => {
                let error = JSON.parse(err);
                if (error.status === 422) {
                    notification.error({ message: error.data.message });
                    let errors = error.data.errors;
                    for (error in errors) {
                        signUpForm.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else {
                    notification.error({
                        message: "Server error. Please try again",
                    });
                }
            });
    };
    const [signUpForm] = Form.useForm();
    const loading = useSelector((state) => state.auth.loading);
    return (
        <FrontendLayout>
            <Row>
                <Col xs={0} md={14}>
                    <img
                        src={bgImg}
                        alt="Signup Background"
                        style={{ width: "100%", height: "100%" }}
                    />
                </Col>
                <Col xs={24} md={10}>
                    <Row
                        justify="center"
                        style={{ height: "100%", paddingTop: "2rem" }}
                    >
                        <Col xs={20}>
                            <Card size="small" style={{ borderRadius: "10px" }}>
                                <Title
                                    style={{
                                        color: grey[4],
                                        textAlign: "center",
                                    }}
                                    level={5}
                                >
                                    Sign Up To Purchase Stuff
                                </Title>
                                <Form
                                    size="small"
                                    layout="vertical"
                                    form={signUpForm}
                                    onFinish={onSignup}
                                >
                                    <Item
                                        name="name"
                                        label={
                                            <Title
                                                level={5}
                                                style={{ color: grey[5] }}
                                            >
                                                Name
                                            </Title>
                                        }
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Your name is required",
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                            placeholder="John Doe"
                                        />
                                    </Item>
                                    <Row gutter={4}>
                                        <Col xs={24} md={12}>
                                            <Item
                                                name="email"
                                                label={
                                                    <Title
                                                        level={5}
                                                        style={{
                                                            color: grey[5],
                                                        }}
                                                    >
                                                        Email Address
                                                    </Title>
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "Email address is required",
                                                    },
                                                    {
                                                        type: "email",
                                                        message:
                                                            "Must be a valid email address",
                                                    },
                                                ]}
                                            >
                                                <Input
                                                    size="large"
                                                    placeholder="email@domain.com"
                                                />
                                            </Item>
                                        </Col>
                                        <Col xs={24} md={12}>
                                            <Item
                                                name="phone"
                                                label={
                                                    <Title
                                                        level={5}
                                                        style={{
                                                            color: grey[5],
                                                        }}
                                                    >
                                                        Phone
                                                    </Title>
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "Your phone number is required",
                                                    },
                                                    {
                                                        validator: (
                                                            _,
                                                            value
                                                        ) => {
                                                            if (
                                                                !value ||
                                                                phone(value, {
                                                                    country:
                                                                        "NG",
                                                                }).isValid
                                                            ) {
                                                                return Promise.resolve();
                                                            }
                                                            return Promise.reject(
                                                                "Must be a valid phone number"
                                                            );
                                                        },
                                                    },
                                                ]}
                                            >
                                                <Input
                                                    size="large"
                                                    placeholder="+234******88"
                                                />
                                            </Item>
                                        </Col>
                                        <Col xs={24} md={12}>
                                            <Item
                                                name="password"
                                                label={
                                                    <Title
                                                        level={5}
                                                        style={{
                                                            color: grey[5],
                                                        }}
                                                    >
                                                        Password
                                                    </Title>
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "Please enter your password",
                                                    },
                                                    {
                                                        min: 8,
                                                        message:
                                                            "Mininum of 8 characters",
                                                    },
                                                ]}
                                            >
                                                <Input.Password
                                                    size="large"
                                                    placeholder="***********"
                                                />
                                            </Item>
                                        </Col>
                                        <Col xs={24} md={12}>
                                            <Item
                                                name="password_confirmation"
                                                dependencies={["password"]}
                                                label={
                                                    <Title
                                                        level={5}
                                                        style={{
                                                            color: grey[5],
                                                        }}
                                                    >
                                                        Confirm Password
                                                    </Title>
                                                }
                                                rules={[
                                                    {
                                                        required: true,
                                                        message:
                                                            "Confirm your password",
                                                    },
                                                    ({ getFieldValue }) => ({
                                                        validator: (
                                                            _,
                                                            value
                                                        ) => {
                                                            if (
                                                                !value ||
                                                                getFieldValue(
                                                                    "password"
                                                                ) === value
                                                            ) {
                                                                return Promise.resolve();
                                                            }
                                                            return Promise.reject(
                                                                "Must be equal to the password"
                                                            );
                                                        },
                                                    }),
                                                ]}
                                            >
                                                <Input.Password
                                                    size="large"
                                                    placeholder="***********"
                                                />
                                            </Item>
                                        </Col>
                                    </Row>

                                    <Item>
                                        <Button
                                            loading={loading}
                                            htmlType="submit"
                                            size="large"
                                            block
                                            type="primary"
                                            shape="round"
                                        >
                                            Sign Up
                                        </Button>
                                    </Item>
                                </Form>
                                <Row justify="space-between">
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/forgot-password"
                                    >
                                        Forgot Password?
                                    </Link>
                                    <Link
                                        style={{ color: grey[5] }}
                                        to="/login"
                                    >
                                        Already have an account? Sign In
                                    </Link>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </FrontendLayout>
    );
}
