import React, { useEffect } from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Row,
    Col,
    Card,
    Typography,
    Form,
    Input,
    Select,
    Button,
    notification,
} from "antd";
import { grey, orange } from "@ant-design/colors";
import { useDispatch, useSelector } from "react-redux";
import { getAllBanks } from "../../features/transactions/transactionSlice";
import { updateBankSettings } from "../../features/auth/authSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;
const { Option } = Select;

const { Item } = Form;
function BankDetails() {
    usePageTitle("Setup bank details");
    const dispatch = useDispatch();
    const [form] = Form.useForm();
    const banks = useSelector((state) => state.transaction.banks);
    const user = useSelector((state) => state.auth.user);
    useEffect(() => {
        dispatch(getAllBanks());
        form.setFields([
            { name: "account_number", value: user.account_number },
            { name: "bank", value: user.bank_id },
        ]);
    }, [dispatch, user, form]);

    const onSubmit = ({ bank, account_number }) => {
        dispatch(updateBankSettings({ bank, account_number }))
            .unwrap()
            .then(() => [
                notification.success({
                    message: "Bank settings updated successfully",
                }),
            ])
            .catch((err) => {
                err = JSON.parse(err);
                if (err.status === 422) {
                    notification.error({ message: err.data.message });
                    const errors = err.data.errors;
                    for (const error in errors) {
                        form.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else {
                    notification.error({ message: "Server error. Try again" });
                }
            });
    };
    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card bordered style={{ borderRadius: "10px" }}>
                        <Title
                            level={5}
                            style={{ textAlign: "center", color: grey[5] }}
                        >
                            Update Bank Details
                        </Title>
                        <Form onFinish={onSubmit} layout="vertical" form={form}>
                            <Item
                                rules={[
                                    {
                                        required: true,
                                        message: "Enter your account number",
                                    },
                                ]}
                                name="account_number"
                                label="Account Number"
                            >
                                <Input />
                            </Item>
                            <Item
                                rules={[
                                    {
                                        required: true,
                                        message: "Select your bank",
                                    },
                                ]}
                                name="bank"
                                label="Bank Name"
                            >
                                <Select placeholder="Select your bank">
                                    {banks.map((bank) => (
                                        <Option key={bank.id} value={bank.id}>
                                            {bank.name}
                                        </Option>
                                    ))}
                                </Select>
                            </Item>
                            <Item>
                                <Button
                                    style={{
                                        border: 0,
                                        backgroundColor: orange[5],
                                        color: "white",
                                    }}
                                    htmlType="submit"
                                >
                                    Update Bank Settings
                                </Button>
                            </Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default BankDetails;
