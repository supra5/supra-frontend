import React from "react";
import { Typography, Row, Col } from "antd";
import AuthLayout from "../../layouts/AuthLayout";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { v4 as uuid } from "uuid";
import {
    MoneyCollectFilled,
    DollarCircleFilled,
    PoundCircleFilled,
    PhoneOutlined,
    GlobalOutlined,
    ThunderboltFilled,
    WifiOutlined,
    KeyOutlined,
} from "@ant-design/icons";

import { blue, green, grey, orange, purple, red } from "@ant-design/colors";
import { formatNumber } from "../../utils/helpers";
import {
    CardWidget,
    TitleWidget,
    StatusWidget,
} from "../../components/Widgets";
import { usePageTitle } from "../../hooks/usePageTitle";
import MyLimitedTransactionTable from "../../components/MyLimitedTransactionTable";

const { Title, Text } = Typography;

const links = [
    {
        name: "Buy Airtime",
        link: "/merchant/buy-airtime",
        key: uuid(),
        icon: (
            <PhoneOutlined
                style={{
                    fontSize: "1.5em",
                    color: orange[5],
                }}
            />
        ),
    },
    {
        name: "Buy Data",
        link: "/merchant/buy-data",
        key: uuid(),
        icon: (
            <WifiOutlined
                style={{
                    fontSize: "1.5em",
                    color: blue[5],
                }}
            />
        ),
    },
    {
        name: "Buy Cable",
        link: "/merchant/buy-cable",
        key: uuid(),
        icon: (
            <GlobalOutlined
                style={{
                    fontSize: "1.5em",
                    color: green[5],
                }}
            />
        ),
    },

    {
        name: "Recharge power subscription",
        link: "/merchant/buy-power",
        key: uuid(),
        icon: (
            <ThunderboltFilled
                style={{
                    fontSize: "1.5em",
                    color: purple[5],
                }}
            />
        ),
    },
    {
        name: "Buy WAEC scratch card",
        link: "/merchant/buy-waec-scratchcard",
        key: uuid(),
        icon: <KeyOutlined style={{ fontSize: "1.5em", color: purple[5] }} />,
    },
];

const widgetSpacing = {
    xs: 12,
    lg: 4,
    style: { marginBottom: "2em" },
};

const menus = links.map((link, index) => (
    <Col key={link.key} {...widgetSpacing}>
        <Link to={link.link}>
            <CardWidget colorKey={index}>
                <Row gutter={5}>
                    <Col xs={8}>{link.icon}</Col>
                    <Col xs={16}>
                        <Text
                            style={{
                                color: grey[3],
                                textAlign: "center",
                            }}
                        >
                            {link.name}
                        </Text>
                    </Col>
                </Row>
            </CardWidget>
        </Link>
    </Col>
));

function Dashboard() {
    usePageTitle("Dashboard");

    const user = useSelector((state) => state.auth.user);

    const generalStatus = [
        {
            title: "Total Sales",
            key: uuid(),
            icon: (
                <DollarCircleFilled
                    style={{ fontSize: "1.4em", color: red[5] }}
                />
            ),
            value: (
                <>
                    &#8358;
                    {formatNumber(user.earning?.total_sales) || "0.0"}
                </>
            ),
        },
        {
            title: "Withdrawable Earnings",
            key: uuid(),
            icon: (
                <MoneyCollectFilled
                    style={{
                        fontSize: "1.4em",
                        color: blue[5],
                    }}
                />
            ),
            value: (
                <>
                    &#8358;
                    {formatNumber(user.earning?.my_earnings) || "0.0"}
                </>
            ),
        },
        {
            title: "Total Earnings",
            key: uuid(),
            icon: (
                <PoundCircleFilled
                    style={{
                        fontSize: "1.5em",
                        color: green[5],
                    }}
                />
            ),
            value: (
                <>
                    &#8358;
                    {formatNumber(user.earning?.total_earnings) || "0.0"}
                </>
            ),
        },
    ];

    return (
        <AuthLayout>
            <div style={{ padding: "1em" }}>
                <Title style={{ color: grey[4] }} level={4}>
                    Dashboard
                </Title>

                <Row gutter={5}>
                    {generalStatus.map((status) => (
                        <Col key={status.key} {...widgetSpacing}>
                            <StatusWidget
                                icon={status.icon}
                                title={status.title}
                                value={status.value}
                            />
                        </Col>
                    ))}

                    <Col {...widgetSpacing}>
                        <Link to="/merchant/my-earnings">
                            <CardWidget colorKey={3}>
                                <Row gutter={1}>
                                    <Col xs={6}>
                                        <PoundCircleFilled
                                            style={{
                                                fontSize: "1.5em",
                                                color: orange[5],
                                            }}
                                        />
                                    </Col>
                                    <Col xs={18}>
                                        <TitleWidget> Earnings</TitleWidget>
                                    </Col>
                                </Row>
                                Withdraw Earnings
                            </CardWidget>
                        </Link>
                    </Col>
                </Row>
                <hr />

                <Row gutter={3}>{menus}</Row>

                <MyLimitedTransactionTable />
            </div>
        </AuthLayout>
    );
}

export default Dashboard;
