import React from "react";
import AuthLayout from "../../layouts/AuthLayout";
import {
    Row,
    Col,
    Card,
    Typography,
    Form,
    InputNumber,
    Button,
    notification,
} from "antd";
import { blue, grey, orange } from "@ant-design/colors";
import { useSelector, useDispatch } from "react-redux";
import { withdrawEarning } from "../../features/auth/authSlice";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title, Text } = Typography;
const { Item } = Form;

function MyEarnings() {
    usePageTitle("Your Earnings");
    const loading = useSelector((state) => state.auth.loading);
    const [form] = Form.useForm();
    const dispatch = useDispatch();
    const myEarning = useSelector(
        (state) => state.auth.user.earning.my_earnings
    );

    const onWithdraw = ({ amount }) => {
        dispatch(withdrawEarning({ amount }))
            .unwrap()
            .then(() => {
                notification.success({
                    message: "Request processing",
                    description: "You should get your money soon",
                });
            })
            .catch((err) => {
                err = JSON.parse(err);
                if (err.status === 422) {
                    notification.error({ message: err.data.message });
                    const errors = err.data.errors;
                    for (const error in errors) {
                        form.setFields([
                            { name: error, errors: errors[error] },
                        ]);
                    }
                } else if (err.status === 400) {
                    notification.error({ message: err.data.message });
                } else {
                    notification.error({ message: "Server error. Try again" });
                }
            });
    };

    return (
        <AuthLayout>
            <Row style={{ marginTop: "3em" }} justify={"center"}>
                <Col xs={20} lg={12}>
                    <Card
                        bordered
                        style={{
                            borderRadius: "10px",
                            backgroundColor: blue[0],
                        }}
                    >
                        <Title
                            level={3}
                            style={{
                                textAlign: "center",
                                color: grey[4],
                            }}
                        >
                            Withdraw Earnings
                        </Title>
                        <Text>Withdrawable earnings: {myEarning}</Text>
                        <Form
                            onFinish={onWithdraw}
                            layout="vertical"
                            form={form}
                        >
                            <Item
                                name="amount"
                                help="Minimum amount withdrawable is 3000 naira"
                                rules={[
                                    {
                                        required: true,
                                        message: "Field required",
                                    },
                                ]}
                                label="Amount"
                            >
                                <InputNumber
                                    style={{ width: "100%" }}
                                    size="large"
                                />
                            </Item>
                            <Item>
                                <Button
                                    loading={loading}
                                    style={{
                                        border: 0,
                                        backgroundColor: orange[5],
                                        color: "white",
                                    }}
                                    htmlType="submit"
                                >
                                    Withdraw
                                </Button>
                            </Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default MyEarnings;
