import React from "react";
import AuthLayout from "../../layouts/AuthLayout";
import { generateLink } from "../../features/auth/authSlice";
import {
    Row,
    Col,
    Card,
    Typography,
    Input,
    notification,
    Button,
    Form,
} from "antd";
import { blue, grey, orange } from "@ant-design/colors";
import { CopyOutlined } from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";
import { usePageTitle } from "../../hooks/usePageTitle";

const { Title } = Typography;

function MyLink() {
    usePageTitle("My Link");
    const user = useSelector((state) => state.auth.user);
    const loading = useSelector((state) => state.auth.loading);
    const dispatch = useDispatch();

    const copyLink = () => {
        if (!user.link) {
            notification.warning({
                message: "You have no link yet",
                description: "Generate one!",
            });
            return;
        }
        navigator.clipboard.writeText(user.link);
        notification.info({
            message: "Link copied!",
            description: "You can share this link amongst your customers",
        });
    };

    const onGenerateLink = () => {
        if (
            user.link &&
            !window.confirm(
                "Generating this link will render current one invalid. Continue?"
            )
        ) {
            return;
        }
        dispatch(generateLink())
            .unwrap()
            .then(() => {
                notification.success({
                    message: "Link generated successfully",
                    description:
                        "You can share this link amongst your customers",
                });
            })
            .catch(() => {
                notification.error({
                    message: "There was an error generating you link.",
                });
            });
    };
    return (
        <AuthLayout>
            <Row justify="center">
                <Col xs={24} md={12} lg={10}>
                    <Card
                        style={{
                            borderRadius: 5,
                            marginTop: "2em",
                            backgroundColor: blue[0],
                        }}
                    >
                        <Title
                            style={{ color: grey[5], textAlign: "center" }}
                            level={4}
                        >
                            My Link
                        </Title>
                        <Form onFinish={onGenerateLink}>
                            <Form.Item>
                                <Input
                                    value={user.link}
                                    placeholder="Genrate your link"
                                    readOnly
                                    size="large"
                                    addonAfter={
                                        <CopyOutlined
                                            onClick={copyLink}
                                            size="large"
                                            style={{ cursor: "pointer" }}
                                        />
                                    }
                                />
                            </Form.Item>
                            <Form.Item>
                                <Button
                                    htmlType="submit"
                                    loading={loading}
                                    type="primary"
                                    style={{
                                        backgroundColor: orange[4],
                                        border: 0,
                                    }}
                                    block
                                >
                                    {!user.link
                                        ? "Generate Link"
                                        : "Regenerate Link"}
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </AuthLayout>
    );
}

export default MyLink;
