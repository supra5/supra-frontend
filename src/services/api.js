import axios from "axios";
import store from "../app/store";

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

api.interceptors.request.use((config) => {
  const token = store.getState().auth.token;
  config.headers = {
    Authorization: token ? `Bearer: ${token}` : null,
    Accept: "application/json",
  };
  return config;
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      store.dispatch({ type: "auth/removeUser" });
    }
    return Promise.reject(error);
  }
);

export default api;
