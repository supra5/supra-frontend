export const formatNumber = (number) => {
  if (!number) return null;
  if (isNaN(number)) return null;
  return `${new Intl.NumberFormat().format(number)}`;
};
