export const getItem = (key) => {
  try {
    return JSON.parse(localStorage.getItem(key));
  } catch (err) {
    return null;
  }
};

export const saveItem = (key, item) => {
  try {
    let stringItem = JSON.stringify(item);
    localStorage.setItem(key, stringItem);
    return true;
  } catch (err) {
    return false;
  }
};

export const removeItem = (key) => {
  try {
    localStorage.removeItem(key);
    return true;
  } catch (err) {
    return false;
  }
};
